#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include "../include/defs.h"

/*------------------------------------------------------------
FUNCTION  DestroyDB(int argc, char *argv[])

ERRORS REPORTED:
       CANT_DELETE_OPENED_DB
       CANT_DELETE_NON_EXIST_DB

IMPLEMENTATION NOTES (IF ANY):
       It destroys all the files alongwith the DB directory.
       DB must be closed prior to deletion
------------------------------------------------------------*/
int DestroyDB(int argc, char *argv[]) {

    struct stat st;
    char deleteDBName[PATH_SIZE], tempCurrOpenDB[PATH_SIZE];
    strcpy(deleteDBName, MINIREL_HOME);
    strcat(deleteDBName, "/");
    strcat(deleteDBName, argv[1]);

    strcpy(tempCurrOpenDB, currOpenDB);

    //printf("curr : %s delete: %s", currOpenDB, deleteDBName);
    for (int i = 0; i < strlen(currOpenDB); ++i) {
        if (tempCurrOpenDB[i] != "/")
            tempCurrOpenDB[i] = toupper(tempCurrOpenDB[i]);
    }

    for (int i = 0; i < strlen(deleteDBName); ++i) {
        if (deleteDBName[i] != "/")
            deleteDBName[i] = toupper(deleteDBName[i]);
    }


    if (isDBOpen() == OK && !strcmp(tempCurrOpenDB, deleteDBName)) {
        ErrorMsgs(CANT_DELETE_OPENED_DB, 1);
        return NOTOK;
    }
    // deleting DB directory , it should exist
    if (stat(deleteDBName, &st) == -1) {
        ErrorMsgs(CANT_DELETE_NON_EXIST_DB, 1);
        return NOTOK;
    }

    DIR *directory = opendir(deleteDBName);
    size_t dbname_path_len = strlen(deleteDBName);

    if (directory) {
        struct dirent *dirent_pointer;

        while (dirent_pointer = readdir(directory)) {
            char *buffer;
            size_t filename_len;

            /* skip "." & ".."  */
            if (!strcmp(dirent_pointer->d_name, ".") || !strcmp(dirent_pointer->d_name, "..")) {
                continue;
            }

            filename_len = dbname_path_len + strlen(dirent_pointer->d_name) + 2;
            buffer = malloc(filename_len);

            if (buffer) {
                snprintf(buffer, filename_len, "%s/%s", deleteDBName, dirent_pointer->d_name);
                remove(buffer);
                free(buffer);
            }
        }
        closedir(directory);
    }
    rmdir(deleteDBName);
}



