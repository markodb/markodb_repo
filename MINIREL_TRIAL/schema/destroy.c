#include "../include/defs.h"

/*------------------------------------------------------------
FUNCTION  DestroyDB(int argc, char *argv[])

ERRORS REPORTED:
       DB_NOT_OPEN
       USER_CANT_DESTROY_METADATA
       UNABLE_TO_DELETE_REL
       REL_NOEXIST

IMPLEMENTATION NOTES (IF ANY):
       It destroys all the relation file if it exists.
       It also removes enteries from metadata.
------------------------------------------------------------*/
int Destroy(int argc, char *argv[]) {
    if (isDBOpen() == NOTOK) {
        ErrorMsgs(DB_NOT_OPEN, 1);
        return NOTOK;
    }

    if (access(getFileName(argv[1]), F_OK) != -1) {
        // file exists
        int relNum = FindRelNum(argv[1]);

        if (!strcmp(RELCAT, argv[1]) || !strcmp(ATTRCAT, argv[1])) {
            ErrorMsgs(USER_CANT_DESTROY_METADATA, 1);
            return NOTOK;
        }

        if (relNum != NOTOK) {
            pageValid[relNum] = 0;
        }
        DeleteRelcatRecord(argv[1]);
        DeleteAttrcatRecords(argv[1]);

        if (remove(getFileName(argv[1])) == 0) {
            return OK;
        }
        ErrorMsgs(UNABLE_TO_DELETE_REL, 1);
        return NOTOK;
    } else {
        ErrorMsgs(REL_NOEXIST, 1);
        return NOTOK;
    }

}


