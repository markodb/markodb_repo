#include "../include/defs.h"

/*
 *
 * it will open rel if not open
 *
 */
/*------------------------------------------------------------
FUNCTION Print(int argc, char *argv[])

PARAMETER DESCRIPTION:
       argc
       argv

GLOABL VARIABLE MODIFIED:
      "currOpenDB" variable is set to MINIREL_HOME

ERRORS REPORTED:
       DB_NOT_OPEN
       USER_CANT_PRINT_METADATA

IMPLEMENTATION NOTES (IF ANY):
       This function prints the relation tuples in nice formatted way

------------------------------------------------------------*/
void Print(int argc, char *argv[]) {

    if (isDBOpen() == NOTOK) {
        ErrorMsgs(DB_NOT_OPEN, 1);
        return;
    }

    int relNum;
    char dashArray[150];
    dashArray[0] = '\0';

    if (!strcmp(argv[1], RELCAT) ){
        relNum =0;
    }
    else if(!strcmp(argv[1], ATTRCAT)) {
            relNum=1;
    }
    else{
        relNum = OpenRel(argv[1]);
    }

    if(relNum == NOTOK){
        return ;
    }

    FlushPage(relNum);

    Rid startRid, foundRid;
    char *fetchedTuple = (char *) malloc((size_t) relCache[relNum].recordLength);
    attrCatNode *list;

    if (pageBuffer[relNum].pid != 0) {
        FlushPage(relNum);
        ReadPage(relNum, 0);
    }

    startRid.pid = 0;
    startRid.slotnum = 0;

    printf("\n");
    for (int i = 0; i <= ((relCache[relNum].attributeCount * 18) / 2) - 20; ++i) {
        printf(" ");

    }

    printf(BLU "## %s Relation ##" RESET, (argv[1]));
    printf("\n");

    list = relCache[relNum].attrList;
    while (list) {
        printf(BLU "%-18s" RESET, list->attrName);
        list = list->next;
        strcat(dashArray, "==================");
    }
    printf(BLK "\n%s" RESET, dashArray);

    do {

        if (GetNextRec(relNum, &startRid, &foundRid, fetchedTuple) == NOTOK) {
            free(fetchedTuple);
            return;
        }

        print_tuple(relNum, fetchedTuple);

        // getNext REC on next RID
        if (foundRid.slotnum == relCache[relNum].recordsPerPage - 1 && foundRid.pid + 1 < relCache[relNum].pageCount) {
            // when foundRid is last slotNo and there exist next page
            startRid.slotnum = 0;
            startRid.pid = foundRid.pid + 1;
        } else if (foundRid.slotnum < relCache[relNum].recordsPerPage - 1) {
            // when foundRid is not last slotNo and no need to check if there exist next page
            startRid.slotnum = foundRid.slotnum + 1;
            startRid.pid = foundRid.pid;
        } else {
            // when foundRid is last slotNo and there doesn't exist next page
            break;
        }

    } while (1);

    return;
}

