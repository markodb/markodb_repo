#include "../include/defs.h"

/*------------------------------------------------------------
FUNCTION int CloseDB(int argc, char *argv[])

PARAMETER DESCRIPTION:
       relNum   : slot number in relCache
       pid      : required page Number

GLOABL VARIABLE MODIFIED:
      "currOpenDB" variable is set to MINIREL_HOME

ERRORS REPORTED:
       DB_NOT_OPEN

IMPLEMENTATION NOTES (IF ANY):
       This function closes all the relations opened in given DB. It sets the currentPath to MINIREL_HOME

------------------------------------------------------------*/
int CloseDB(int argc, char *argv[]) {
    if (isDBOpen() == NOTOK) {
        ErrorMsgs(DB_NOT_OPEN, 1);
        return NOTOK;
    }
    // it will first close opened relations and then close cats.
    CloseCats();

    char logmsg[PATH_SIZE];
    logmsg[0]= '\0';
    strcpy(logmsg," >>>> Closing DataBase : ");
    strcat(logmsg, currOpenDB);
    LOG(logmsg);

    // setting the working directory to original directory from which MINREL is invoked
    SetDBPath(MINIREL_HOME);
    return OK;
}


