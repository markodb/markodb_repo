#include "../include/defs.h"

/*------------------------------------------------------------
FUNCTION OpenDB(int argc, char *argv[])

PARAMETER DESCRIPTION:
       relNum   : slot number in relCache
       pid      : required page Number

GLOABL VARIABLE MODIFIED:
      "currOpenDB" variable is set to "MINIREL_HOME/DBName"

ERRORS REPORTED:
       CANT_OPEN_NESTED_DB
       DB_NOT_EXIST

IMPLEMENTATION NOTES (IF ANY):
       It loads relcache and attribute cache for given DBName in argv.
------------------------------------------------------------*/
int OpenDB(int argc, char *argv[]) {
    struct stat st;

    if (isDBOpen() == OK) {
        ErrorMsgs(CANT_OPEN_NESTED_DB, 1);
        return NOTOK;
    }

    if (stat(getFileName(argv[1]), &st) == -1) {
        ErrorMsgs(DB_NOT_EXIST, 1);
        return NOTOK;
    }

    // opening the cats and setting working directory to current DB
    SetDBPath(getFileName(argv[1]));
    OpenCats();

    char logmsg[PATH_SIZE];
    logmsg[0]= '\0';
    strcpy(logmsg," >>>> Opening DataBase : ");
    strcat(logmsg, currOpenDB);
    LOG(logmsg);
    return OK;
}



