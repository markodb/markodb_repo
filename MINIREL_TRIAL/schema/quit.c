#include "../include/defs.h"

/*
 *
 * Db should be closed , if not then close it and then terminate the programs.
 *
 */
int Quit(int argc, char *argv[]) {
    if (!strcmp(currOpenDB, MINIREL_HOME)) {
        printf(BLU "\nGoodbye from MINIREL\n" RESET);
        LOG(">>> CLOSING MINIREL SESSION <<<");
        LOG("--------------------------------------------------------------\n");
        exit(0);
    }

    char **array = (char **) malloc(2 * sizeof(char *));
    int i;

    for (i = 0; i < 2; i++)
        array[i] = (char *) malloc(MAX_ATTRNAME_SIZE);

    strcpy(array[0], "closedb");
    strcpy(array[1], NIL);

    CloseDB(1, array);
    printf(BLU "\nGoodbye from MINIREL\n" RESET);

    LOG(">>> CLOSING MINIREL SESSION <<<");
    LOG("--------------------------------------------------------------\n");
    fclose(logFilePtr);
    exit(0);
}


