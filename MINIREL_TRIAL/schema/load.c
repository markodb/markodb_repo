#include "../include/defs.h"


/*------------------------------------------------------------
FUNCTION  Load(int argc, char *argv[])

ERRORS REPORTED:
       DB_NOT_EXIST
       LOAD_FILE_MISSING
       USER_CANT_UPDATE_METADATA
       CANT_LOAD_REL_NOT_EMPTY

IMPLEMENTATION NOTES (IF ANY):
       It loads already existing empty relation(i.e having 0 tuples) from Data File,
       Data File Path must absolute.
------------------------------------------------------------*/
void Load(int argc, char *argv[]) {
    if (isDBOpen() == NOTOK) {
        ErrorMsgs(DB_NOT_OPEN, 1);
        return;
    }

    int relNo;
    struct stat st;

    // Open required relation
    relNo = FindRelNum(argv[1]);

    if (stat(argv[2], &st) == -1) {
        ErrorMsgs(LOAD_FILE_MISSING, 1);
        return;
    }

    if (!strcmp(argv[1], RELCAT) || !strcmp(argv[1], ATTRCAT)) {
        ErrorMsgs(USER_CANT_UPDATE_METADATA, 1);
        return;
    }

    if (relNo == NOTOK) {
        relNo = OpenRel(argv[1]);
    }

    if(relNo == NOTOK){
        return ;
    }

    if (relCache[relNo].recordCount != 0) {
        ErrorMsgs(CANT_LOAD_REL_NOT_EMPTY, 1);
        return;
    }

    // supress error message for redundant tuple
    isSelfJoin = 1;

    char *tuple = (char *) malloc((size_t) relCache[relNo].recordLength);
    FILE *loadFile = fopen(argv[2], "r");
    size_t count = 0, rowNo = 0;

    count = fread(tuple, 1, (size_t) relCache[relNo].recordLength, loadFile);

    while (count) {

        InsertRec(relNo, tuple);
        rowNo++;
        count = fread(tuple, 1, (size_t) relCache[relNo].recordLength, loadFile);

    }

    // removing supress error message for redundant tuple
    isSelfJoin = 0;

    // flush last page loaded, either it will be closed when closing all cats
    FlushPage(relNo);

    fclose(loadFile);
    free(tuple);
}


