#include "../include/defs.h"

/*------------------------------------------------------------
FUNCTION Create(int argc, char *argv[])

PARAMETER DESCRIPTION:
       argc:
       argv:

ERRORS REPORTED:
       DB_NOT_OPEN
       USER_CANT_CREATE_METADATA
       RELATION_ALREADY_EXIST

IMPLEMENTATION NOTES (IF ANY):
       This function creates a relation file in current DB, and also
       adds metadata records accordingly.

------------------------------------------------------------*/
int Create(int argc, char *argv[]) {

    if (isDBOpen() == NOTOK) {
        ErrorMsgs(DB_NOT_OPEN, 1);
        return NOTOK;
    }

    if (!strcmp(argv[1], RELCAT) || !strcmp(argv[1], ATTRCAT)) {
        ErrorMsgs(USER_CANT_CREATE_METADATA, 1);
        NOTOK;
    }

    //  checking if Relation already exist
    if (access(getFileName(argv[1]), F_OK) != -1) {
        ErrorMsgs(RELATION_ALREADY_EXIST, 1);
        return NOTOK;
    }

    //forming filename : /DBPATH/RelName
    FILE *file = fopen(getFileName(argv[1]), "a");

    char *PageBuffer = (char *) calloc(1, PAGESIZE);

    int totalRelationAttributes = (argc - 2) / 2;
    size_t byteWrittenCount = 0;

    //obj1 of RelCat
    relRecord relObj1;
    relObj1.attributeCount = totalRelationAttributes;
    relObj1.pageCount = 1;
    relObj1.recordCount = 0;
    relObj1.recordsPerPage = getTotalSlots(getRelationRecordLength(argv));
    relObj1.recordLength = getRelationRecordLength(argv);
    if (strlen(argv[1]) > RELNAME_SIZE - 1) {
        ErrorMsgs(REL_NAME_LENGTH_EXCEED, 1);
        return NOTOK;
    }
    strcpy(relObj1.relationName, argv[1]);

    // obj1 of AttrCat
    attrRecord *attrRecord1 = setAttrFields(argv);

    // insert one record in relcat and relationAttributeCount number of records in attrcat
    InsertRec(0, (char *) &relObj1);
    for (int i = 0; i < totalRelationAttributes; i++) {
        InsertRec(1, (char *) (attrRecord1 + i));
    }

    //writing to file
    byteWrittenCount = fwrite(PageBuffer, 1, PAGESIZE, file);
    fflush(file);

    // opening Rel for further operations
    OpenRel(argv[1]);

    //freeing resources
    free(PageBuffer);
    free(attrRecord1);
    fclose(file);

    if (byteWrittenCount < PAGESIZE)
        return NOTOK;

    return OK;
}


