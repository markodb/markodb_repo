#include "../include/defs.h"

/*
 * TODo creates a new db , load catalogs with appropiate initial information,
 Check if DB with given pathname exists
 	If Yes: throw error
 	If No: create DB with given pathname
*/
/*------------------------------------------------------------
FUNCTION  CreateDB(int argc, char *argv[])

ERRORS REPORTED:
       CANT_CREATE_NESTED_DB

IMPLEMENTATION NOTES (IF ANY):
       It creates DB and also creates Metadata relations in the
       "MINIREL_HOME/DB" directory

------------------------------------------------------------*/
int CreateDB(int argc, char *argv[]) {

    if (isDBOpen() == OK) {
        ErrorMsgs(CANT_CREATE_NESTED_DB, 1);
        return NOTOK;
    }

    struct stat st;
    // creating DB directory , it should not exist already
    if (stat(getFileName(argv[1]), &st) == -1) {
        mkdir(getFileName(argv[1]), 0700);
        printf("\nDatabase Created! : %s", getFileName(argv[1]));
        SetDBPath(getFileName(argv[1]));
        CreateCats();
        SetDBPath(MINIREL_HOME);     // to compensate for openDb
    } else if (stat(getFileName(argv[1]), &st) == 0) {
        ErrorMsgs(DB_ALREADY_EXIST, 1);
        printf("\nConflicting Database Names : %s", argv[1]);
        return 0;
    }
}

void SetDBPath(char * name) {
    strcpy(currOpenDB, name);
}
