#include "globals.h"
/*
This is the basic definition file.
*/
/****************************
       STANDARD HEADERS
 ****************************/
#define BLK   "\x1B[30m"
#define RED   "\x1B[31m"
#define GRN   "\x1B[32m"
#define YEL   "\x1B[33m"
#define BLU   "\x1B[34m"
#define MAG   "\x1B[35m"
#define CYN   "\x1B[36m"
#define WHT   "\x1B[37m"
#define YLB   "\x1B[43m"
#define BOLD   "\x1B[1m"
#define RESET "\x1B[0m"

/*************************************************************
		FUNCTION SIGNATURES
*************************************************************/
int CreateCats();

int getSlotMapSize(int);

size_t getRelcatRecordLength();

size_t getAttrcatRecordLength();

int getARGVcount(char *array[]);

char **getAttrcatAttributes();

char **getRelcatAttributes();

size_t getRecordLengthForCreate(char *argv[]);

attrRecord *setAttrFields(char *argv[]);

void SetDBPath(char *);

attrRecord *setAttrFields(char *argv[]);

int CreateAttrCat();

int CreateRelCat();

void relcat_printing();

void attrcat_printing();

void slotMap_printing(char *, int);

char *getFileName(char *filename);

int getCacheIndex(char *);

int ReadPage(int relNum, int pid);

int loadCatsToRelcache();

void updateTimestamp(int relnum);

void OpenCats();

int FindRelNum(char *relName);

void FlushPage(int relNum);

int OpenRel(char *relName);

int getEmptyBufferSlot();

void CloseRel(int relNum);

void CloseCats();

void WriteRec(int relNum, char *recPtr, Rid recRid);

int SlotMap(char *PageBuffer, int slotNumber, int operation);

int GetNextRec(int relNum, Rid *startRid, Rid *foundRid, char *recPtr);

int FindRec(int relNum, Rid *startRid, Rid *foundRid, char *recPtr, int attrType, int attrSize, int attrOffset,
            char *searchTuple, int compOp);

int matchTuple(int attrType, int attrSize, int attrOffset, int compOp, char *fetchedTuple, char *searchTuple);

int getattrOffset(int relNum, char *attrName);

attrCatNode *getAttributeList(char *relName);

int CreateCacheEntryForRelation(char *relName, int relNum);

int CloseDB(int argc, char *argv[]);

char **getRelationAttributes();

size_t getRelationRecordLength(char *argv[]);

void InsertRec(int relNum, char *recPtr);

int Create(int argc, char *argv[]);

int CreateDB(int argc, char *argv[]);

int OpenDB(int argc, char *argv[]);

void DeleteRec(int relNum, Rid *recRid);

void DeleteRelcatRecord(char *argv);

void DeleteAttrcatRecords(char *argv);

int Destroy(int argc, char *argv[]);

int all_prints(int relNum);

int getTotalSlots(int tupleSize);

int validateAttrValues(int attrType, char *value);

char *getRelationRecord(int argc, char *argv[], int relNum);

char **getRelationAttributesWithValues();

int Insert(int argc, char *argv[]);

char **getSelectArgv();

int Select(int argc, char *argv[]);

char **getCreateArgvForSelect(char *relName, int relNum);

int isValueString(char *str);

int isValueInteger(char *str);

int isValueFloat(char *str);

int validateAttributes(int relNum, int argc, char *argv[]);

char **getInsertArgvForSelect(char *relName, int relNum, int attrType, char *value);

void print_tuple(int relNum, char *temp);

int isTupleMatching(char *searchTuple, char *examineTuple, int relNum);

char **getLoadArgv(int);

void Load(int argc, char *argv[]);

void Print(int argc, char *argv[]);

char **getPrintArgv(char *relName);

int validAttrNameAndGetAttrType(attrCatNode *temp, char *attrName);

int getAttrSize(attrCatNode *temp, char *attrName);

int stringComparision(int compOp, char *str1, char *str2);

int floatValueComparision(int compOp, double value1, double value2);

int integerValueComparision(int compOp, int value1, int value2);

char **student1();

char **student2();

char **getJoinArgv();

int Join(int argc, char *argv[]);

int Project(int argc, char *argv[]);

char **getProjectArgv();

void PrintAnyargv(int argc, char *array[]);

char **getDestroyArgv();

int isDBOpen();

void initialize_database();

char **getDeleteArgv();

int Delete(int argc, char *argv[]);

void freeListMemory(attrCatNode *head);

char *getVal(char *temp, attrCatNode *list);

int LRUpageFinder();

int setHomePath();

void write_log(char *msg);