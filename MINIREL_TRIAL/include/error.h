/************************************************************
   This file contains the error constants that are
   used by the routine that reports error messages.
   As you find error conditions to report, 
   make additions to this file. 
************************************************************/
#define REL_NOEXIST                  101   /* Relation does not exist */
#define ATTR_NOEXIST                 102   /* Attribute does not exist */
#define DB_NOT_EXIST                 103   /* Database does not exist */
#define COMMON_ATTR_NOEXIST          104   /* Common Attr does not exist while joining*/
#define ATTR_NAME_LENGTH_EXCEED      105   /* AttrName length exceeds 20 characters*/
#define REL_NAME_LENGTH_EXCEED       106   /* RelName length exceeds 20 characters*/
#define MAX_CHAR_STR_LENGTH_EXCEED   107   /* Character String exceeds 50 characters*/
#define CATALOGS_CREATION_FAILED     108   /* Catalog creation failed */
#define RELCAT_ALREADY_EXIST         109   /* RelCat already exist */
#define ATTRCAT_ALREADY_EXIST        110   /* AttrCat already exist */
#define RELCAT_NOT_LOADED            111   /* Unable to load Relcat */
#define ATTRCAT_NOT_LOADED           112   /* Unable to load Attrcat */
#define DB_ALREADY_EXIST             113   /* Database already exist */
#define RELATION_NOT_LOADED          114   /* Database already exist */
#define RELATION_ALREADY_EXIST       115   /* Relation already exist */
#define UNABLE_TO_DELETE_REL         116   /* Unable to delete relation */
#define UNABLE_TO_INSERT             117   /* Denied to insert a record in closed relation, open relation before insertion */
#define INVALID_ATTR_LIST            118   /* Attribute list does't match with the schema of stored table */
#define DESTINATION_RELATION_EXIST   119   /* Destination relation already exist */
#define SOURCE_RELATION_NOT_EXIST    120   /* Source relation not exist */
#define TUPLE_ALREADY_EXIST          121   /* Tuple already exist in relation*/
#define UNABLE_TO_FLUSH_PAGE         122   /* Page can not be flushed to disk file */
#define ATTR_VALUES_NOT_VALID        123   /* Attribute value provided by user is invalid */
#define ATTR_NAME_NOT_VALID          124   /* Attribute Name provided by user is invalid */
#define CANNOT_LOAD_FILE             125   /* Cannot load relation, Relation not created */
#define CANT_LOAD_REL_NOT_EMPTY      126   /* Can't Load File to Relation, Relation not Empty */
#define OPEN_REL_BEFORE_PRINT        127   /* Open the relation, before Print command */
#define DB_NOT_OPEN                  128   /* Open the DB, before any operation */
#define USER_CANT_DESTROY_METADATA   129   /* Users can not delete Metadata */
#define USER_CANT_OPEN_METADATA      130   /* Users can load Metadata into memory buffers*/
#define CANT_CREATE_NESTED_DB        131   /* Error: Denied to create nested DB*/
#define CANT_OPEN_NESTED_DB          132   /* Error: Denied to open nested DB*/
#define CANT_DELETE_OPENED_DB        133   /* Error: Denied to delete opened DB*/
#define CANT_DELETE_NON_EXIST_DB     134   /* Error: DB don't exist, Denied to delete DB*/
#define INVALID_ATTR_COUNT           135   /* Error: Attribute count not matching with schema*/
#define UNABLE_CREATE_MINIREL_HOME   136   /* Error: Unable to create Minirel Home*/
#define JOIN_COLUMN_NAME_MISMATCH    137   /* Error: Join column name mismatch*/
#define JOIN_COL_MISMATCH_SCHMA_COL  138   /* Error: Join column name mismatch with schema*/
#define DELETE_COL_MISMATCH          139   /* Error: Delete column is not matching with schema*/
#define DELETE_COL_VALUE_MISMATCH    140   /* Error: Delete column value datatype conflicting with schema*/
#define LOAD_FILE_MISSING            141   /* Error:  Load file missing */
#define USER_CANT_UPDATE_METADATA    142   /* Error: Users can't load metadata*/
#define SELECT_COL_MISMATCH_SCHMA_COL 143  /* Error: Select column name mismatch with schema*/
#define USER_CANT_PRINT_METADATA     144   /* Error: Denied to print metadata */
#define USER_CANT_CREATE_METADATA    145   /* Error: Denied to create metadata relations */

int ErrorMsgs(int errorNum, int printFlag);