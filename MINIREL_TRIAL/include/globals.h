#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <ctype.h>
#include "error.h"

/*************************************************************
		CONSTANTS
*************************************************************/

#define PAGESIZE 4096    /* number of bytes in a page */

#define MAX_RELNAME_SIZE    50
#define MAX_ATTRNAME_SIZE   50
#define RELNAME_SIZE        20        /* max length of a relation name */
#define ATTRNAME_SIZE       20        /* max length of a attribute name */
#define MAXOPEN             20        /* max number of files that can be open at the same time */

#define    OK                0        /* return codes */
#define NOTOK               -1
#define PATH_SIZE          200
#define ATTR_COUNT          50

#define RELCAT        "relcat"        /* name of the relation catalog file */
#define ATTRCAT       "attrcat"       /* name of the attribute catalog file */

#define NIL           "NIL"

#define SET_SLOT_BIT_1 1
#define SET_SLOT_BIT_0 0
#define GET_SLOT_BIT   2

#define LOG(msg) write_log(msg)

/*************************************************************
		TYPE DEFINITIONS
*************************************************************/

/* Rid structure */
typedef struct recid {
    int pid;            // page no. of this record
    int slotnum;        // in the page what is the record no.
} Rid;

/*****************************************************************
        STRUCTURES FOR CATALOGS
 *****************************************************************/

typedef struct relCatRecord {
    int recordLength;                                // Size of each record
    int recordsPerPage;                              // count of Records in a page
    int attributeCount;                              // Count of attribute in a relation
    int recordCount;                                 // Total number of records currently in a relation
    int pageCount;                                   // Total number of pages relation span in
    char relationName[RELNAME_SIZE];                 // Relation name

} relRecord;

typedef struct attrCatRecord {
    int attrOffset;                         // Attribute offset of a attribute of a particular relation
    int attrLength;                         // Attribute length in BYTES
    int attrType;                           // will take one of the value among (String is 0, Integer is 1, Float is 2)
    char attrName[ATTRNAME_SIZE];           // attribute name
    char relationName[RELNAME_SIZE];        // Relation name
} attrRecord;

/*****************************************************************
       STRUCTURES FOR CACHE
 *****************************************************************/

typedef struct attrListNode {
    int attrOffset;                        // Attribute offset of a attribute of a particular relation
    int attrLength;                        // Attribute length in BYTES
    int attrType;                          // will take one of the value among (String, Integer, Float) i.e attrType = String
    char attrName[MAX_RELNAME_SIZE];       // attribute name
    struct attrListNode *next;             // pointer required to make a attribute linkedlist
} attrCatNode;

typedef struct relCache {
    char relationName[MAX_ATTRNAME_SIZE];        // Relation name
    int recordLength;                            // Size of each record
    int recordsPerPage;                          // count of Records in a page
    int attributeCount;                          // Count of attribute in a relation
    int recordCount;                             // Total number of records currently in a relation
    int pageCount;                               // Total number of pages relation span in
    int slotBitmapSize;                          // size of bitmap in each page

    struct timeval time;

    // Additional fields other than relCat attributes
    Rid relcatRid;                     // RID for record holding this relation catalog record
    FILE *filePtr;                     // File descriptor for the open relation, use fileno(FILE *stream) to get this
    int dirty;                         // it is true when relcat record  is updated in the cache
    attrCatNode *attrList;             // head pointer to the Linked list of attribute descriptors

} CacheEntry;

typedef struct Page {
    char pageData[PAGESIZE];
    int dirty;
    int pid;
} Page;


/*************************************************************
		GLOBALS
*************************************************************/
char currOpenDB[PATH_SIZE];

CacheEntry relCache[MAXOPEN];
Page pageBuffer[MAXOPEN];
int pageValid[MAXOPEN];
static char final_output[50];
int isSelfJoin;
char currTime[64];
char LogMsg[200];
FILE *logFilePtr;
char MINIREL_HOME[PATH_SIZE];

