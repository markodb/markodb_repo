#include "../include/defs.h"


/*
 *
 * returns AttrType if attrName exists in relation Schema
 * otherwise returns NOTOK
 *
 */
int validAttrNameAndGetAttrType(attrCatNode *temp, char *attrName) {
    while (temp) {
        if (!strcmp(temp->attrName, attrName)) {
            return temp->attrType;
        }
        temp = temp->next;
    }
    return NOTOK;
}

/*
 *
 * returns AttrSize if attrName exists in relation Schema
 * otherwise returns NOTOK
 *
 */
int getAttrSize(attrCatNode *temp, char *attrName) {
    while (temp) {
        if (!strcmp(temp->attrName, attrName)) {
            return temp->attrLength;
        }
        temp = temp->next;
    }
    return NOTOK;
}

/*
 *
 * "create" argv, for creation of resultant Select table. This argv is used as input argument in
 * Create(int argc, char *argv[]).
 *
 */
char **getCreateArgvForSelect(char *relName, int relNum) {
    char **array = (char **) malloc(ATTR_COUNT * sizeof(char *));
    int i, j;
    attrCatNode *temp = relCache[relNum].attrList;
    char val[5];


    for (i = 0; i < ATTR_COUNT; i++)
        array[i] = (char *) malloc(MAX_ATTRNAME_SIZE);

    strcpy(array[0], "create");
    strcpy(array[1], relName);

    for (j = 2; j <= 2 * relCache[relNum].attributeCount; j = j + 2) {

        strcpy(array[j], temp->attrName);
        if (temp->attrType == 0) {
            sprintf(val, "%s%d", "s", temp->attrLength);
            strcpy(array[j + 1], val);
        } else if (temp->attrType == 1) {
            strcpy(array[j + 1], "i");

        } else if (temp->attrType == 2) {
            strcpy(array[j + 1], "f");

        }
        temp = temp->next;
    }

    strcpy(array[j], "NIL");

    return array;
}


/*------------------------------------------------------------

FUNCTION int Select(int argc, char *argv[])

PARAMETER DESCRIPTION:

 * ------------------------
 * implements relational selection command
 *
 * argv[0] = “select”
 * argv[1] = result relation
 * argv[2] = source relation
 * argv[3] = attribute name
 * argv[4] = operator
 * argv[5] = value
 * argv[argc] = NIL
 *
 * returns: OK upon successfully performed selection
 * NOTOK: otherwise

ERRORS REPORTED:
        *      DB_NOT_OPEN
        *      USER_CANT_OPEN_METADATA
        *      DESTINATION_RELATION_EXIST
        *      SOURCE_RELATION_NOT_EXIST.
        *      SELECT_COL_MISMATCH_SCHMA_COL
        *      ATTR_VALUES_NOT_VALID

GLOBAL VARIABLES MODIFIED:
       None

IMPLEMENTATION NOTES (IF ANY):
        Does Relational Select according to argv , argc.
------------------------------------------------------------*/
int Select(int argc, char *argv[]) {

    if (isDBOpen() == NOTOK) {
        ErrorMsgs(DB_NOT_OPEN, 1);
        return NOTOK;
    }

    if (!strcmp(argv[2], RELCAT) || !strcmp(argv[2], ATTRCAT)) {
        ErrorMsgs(USER_CANT_OPEN_METADATA, 1);
        return NOTOK;
    }

    if (access(getFileName(argv[1]), F_OK) != -1) {
        ErrorMsgs(DESTINATION_RELATION_EXIST, 1);
        return NOTOK;
    } else if (access(getFileName(argv[2]), F_OK) == -1) {
        ErrorMsgs(SOURCE_RELATION_NOT_EXIST, 1);
        return NOTOK;
    }

    // opening source relation
    int sourceRelNum = FindRelNum(argv[2]);
    if (sourceRelNum == NOTOK) {
        sourceRelNum = OpenRel(argv[2]);
    }

    if (getAttrSize(relCache[sourceRelNum].attrList, argv[3]) == NOTOK) {
        ErrorMsgs(SELECT_COL_MISMATCH_SCHMA_COL, 1);
        return NOTOK;
    }

    // inserting the records in Destination Relation
    Rid startRid, foundRid;
    startRid.pid = 0;
    startRid.slotnum = 0;

    // coverting operator to integer to operator value
    int operator = 0;
    memcpy(&operator, argv[4], 2);

    int compOp = operator, destinationRelNum;
    int attrOffset = getattrOffset(sourceRelNum, argv[3]);
    int attrType = validAttrNameAndGetAttrType(relCache[sourceRelNum].attrList, argv[3]);

    //  validates attribute value
    if (validateAttrValues(attrType, argv[5]) == NOTOK) {
        ErrorMsgs(ATTR_VALUES_NOT_VALID, 1);
        return NOTOK;
    }

    // creates destination relation
    Create(relCache[sourceRelNum].attributeCount * 2 + 2, getCreateArgvForSelect(argv[1], sourceRelNum));
    destinationRelNum = OpenRel(argv[1]);

    if (attrType == NOTOK) {
        ErrorMsgs(ATTR_NAME_NOT_VALID, 1);
        return NOTOK;

    }

    // attrSize of Source relation
    int attrSize = getAttrSize(relCache[sourceRelNum].attrList, argv[3]);

    char *sourceRecord = getRelationRecord(relCache[sourceRelNum].attributeCount * 2 + 3,
                                           getInsertArgvForSelect(argv[1], destinationRelNum, attrType, argv[5]),
                                           sourceRelNum);

    char *foundRecord = (char *) malloc((size_t) relCache[sourceRelNum].recordLength);

    while (1) {
        if (FindRec(sourceRelNum, &startRid, &foundRid, foundRecord, attrType, attrSize, attrOffset, sourceRecord,
                    compOp) == OK) {

            InsertRec(destinationRelNum, foundRecord);

            if (foundRid.slotnum == relCache[sourceRelNum].recordsPerPage - 1 &&
                foundRid.pid + 1 < relCache[sourceRelNum].pageCount) {
                // when foundRid is last slotNo and there exist next page
                startRid.slotnum = 0;
                startRid.pid = foundRid.pid + 1;
            } else if (foundRid.slotnum < relCache[sourceRelNum].recordsPerPage - 1) {
                // when foundRid is not last slotNo and no need to check if there exist next page
                startRid.slotnum = foundRid.slotnum + 1;
                startRid.pid = foundRid.pid;
            } else {
                // when foundRid is last slotNo and there doesn't exist next page
                break;
            }

        } else {
            break;
        }

    }
    return OK;
}

/*
 *
 * "Insert" argv, for inserting a record in resultant Select table.
 *
 */
char **getInsertArgvForSelect(char *relName, int relNum, int attrType, char *value) {
    char **array = (char **) malloc(ATTR_COUNT * sizeof(char *));
    int i, j;
    attrCatNode *temp = relCache[relNum].attrList;


    for (i = 0; i < ATTR_COUNT; i++)
        array[i] = (char *) malloc(MAX_ATTRNAME_SIZE);

    strcpy(array[0], "insert");
    strcpy(array[1], relName);

    for (j = 2; j <= 2 * relCache[relNum].attributeCount; j = j + 2) {

        strcpy(array[j], temp->attrName);
        if (temp->attrType == 0) {
            strcpy(array[j + 1], "string");
        } else if (temp->attrType == 1) {
            strcpy(array[j + 1], "1");

        } else if (temp->attrType == 2) {
            strcpy(array[j + 1], "1.0");

        }

        // Setting the particular value of attrType to input value
        if (temp->attrType == attrType) {
            strcpy(array[j + 1], value);
        }
        temp = temp->next;
    }

    strcpy(array[j], "NIL");

    return array;
}

