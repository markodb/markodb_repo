#include "../include/defs.h"

/*------------------------------------------------------------

FUNCTION int Delete(int argc, char *argv[])

PARAMETER DESCRIPTION:
 * argv[0] = “delete”
 * argv[1] = source relation
 * argv[2] = attribute name
 * argv[3] = operator
 * argv[4] = value
 * argv[argc] = NIL

 returns: OK upon successfully performed deletion
          NOTOK: otherwise

ERRORS REPORTED:
       * DB_NOT_OPEN
       * SOURCE_RELATION_NOT_EXIST
       * USER_CANT_DESTROY_METADATA
       * DELETE_COL_MISMATCH
       * DELETE_COL_VALUE_MISMATCH

GLOBAL VARIABLES MODIFIED:
       None.

IMPLEMENTATION NOTES (IF ANY):
       This function deletes the specified records from source relation ,
       according to given attribute name, operator and value

------------------------------------------------------------*/
int Delete(int argc, char *argv[]) {
    // Db should be opened before any operation;
    if (isDBOpen() == NOTOK) {
        ErrorMsgs(DB_NOT_OPEN, 1);
        return NOTOK;
    }

    // source file should present in the db
    if (access(getFileName(argv[1]), F_OK) == -1) {
        ErrorMsgs(SOURCE_RELATION_NOT_EXIST, 1);
        return NOTOK;
    }


    if (!strcmp(RELCAT, argv[1]) || !strcmp(ATTRCAT, argv[1])) {
        ErrorMsgs(USER_CANT_DESTROY_METADATA, 1);
        return NOTOK;
    }

    // opening source rel
    int relNum = OpenRel((argv[1]));


    int sourceAttrType = validAttrNameAndGetAttrType(relCache[relNum].attrList, argv[2]);

    // validating if provided attrname exists in source relation
    if (sourceAttrType == NOTOK) {
        ErrorMsgs(DELETE_COL_MISMATCH, 1);
        return NOTOK;
    }

    attrCatNode *sourceAttrList = relCache[relNum].attrList;

    // validating attrvalue provided in argv
    if (validateAttrValues(sourceAttrType, argv[4])) {
        ErrorMsgs(DELETE_COL_VALUE_MISMATCH, 1);
        return NOTOK;
    }

    Rid startRid, foundRid;
    startRid.pid = 0;
    startRid.slotnum = 0;
    // coverting operator to integer to operator value
    int operator = 0;
    memcpy(&operator, argv[3], 2);

    int compOp = operator;
    int attrOffset = getattrOffset(relNum, argv[2]);
    int attrSize = getAttrSize(relCache[relNum].attrList, argv[2]);

    char *sourceRecord = getRelationRecord(relCache[relNum].attributeCount * 2 + 3,
                                           getInsertArgvForSelect(argv[1], relNum, sourceAttrType, argv[4]), relNum);


    char *foundRecord = (char *) malloc((size_t) relCache[relNum].recordLength);


    while (1) {
        if (FindRec(relNum, &startRid, &foundRid, foundRecord, sourceAttrType, attrSize, attrOffset, sourceRecord,
                    compOp) == OK) {

            DeleteRec(relNum, &foundRid);

            if (foundRid.slotnum == relCache[relNum].recordsPerPage - 1 &&
                foundRid.pid + 1 < relCache[relNum].pageCount) {
                // when foundRid is last slotNo and there exist next page
                startRid.slotnum = 0;
                startRid.pid = foundRid.pid + 1;
            } else if (foundRid.slotnum < relCache[relNum].recordsPerPage - 1) {
                // when foundRid is not last slotNo and no need to check if there exist next page
                startRid.slotnum = foundRid.slotnum + 1;
                startRid.pid = foundRid.pid;
            } else {
                // when foundRid is last slotNo and there doesn't exist next page
                break;
            }

        } else {
            break;
        }
    }

    return OK;
}

