#include "../include/defs.h"

/*
 *
 * "create" argv, for creation of resultant join table. This argv is used as input argument in
 * Create(int argc, char *argv[]).
 *
 */
char **getCreateArgvForJoin(char *destRelName, int relNum1, int relNum2, char *joinCol) {

    char **array = (char **) malloc(ATTR_COUNT * sizeof(char *));
    int i, j, k = 2, attrMatchFlag = 0;
    attrCatNode *temp1 = NULL, *temp2 = NULL;
    char val[5];
    isSelfJoin = (!(strcmp(relCache[relNum1].relationName, relCache[relNum2].relationName)));

    for (i = 0; i < ATTR_COUNT; i++)
        array[i] = (char *) malloc(MAX_ATTRNAME_SIZE);

    strcpy(array[0], "create");
    strcpy(array[1], destRelName);

    temp1 = relCache[relNum1].attrList;
    for (j = 2; j <= 2 * relCache[relNum1].attributeCount; j = j + 2, k = k + 2) {

        if (isSelfJoin) {
            strcpy(array[j], "A");
            strcat(array[j], ".");
            strcat(array[j], temp1->attrName);

        } else {
            strcpy(array[j], relCache[relNum1].relationName);
            strcat(array[j], ".");
            strcat(array[j], temp1->attrName);
        }

        if (temp1->attrType == 0) {
            sprintf(val, "%s%d", "s", temp1->attrLength);
            strcpy(array[j + 1], val);
        } else if (temp1->attrType == 1) {
            strcpy(array[j + 1], "i");

        } else if (temp1->attrType == 2) {
            strcpy(array[j + 1], "f");
        }
        temp1 = temp1->next;
    }

    temp2 = relCache[relNum2].attrList;
    for (j = k; j <= ((2 * relCache[relNum2].attributeCount) + k) && temp2; j = j + 2) {

        temp1 = relCache[relNum1].attrList;
        while (temp1) {
            if (!strcmp(temp1->attrName, temp2->attrName) && !strcmp(temp2->attrName, joinCol)) {
                attrMatchFlag = 1;
                temp2 = temp2->next;
                break;
            }
            temp1 = temp1->next;
        }

        if (attrMatchFlag == 1) {
            attrMatchFlag = 2;
            j = j - 2;
            continue;
        }

        if (isSelfJoin) {
            strcpy(array[j], "B");
            strcat(array[j], ".");
            strcat(array[j], temp2->attrName);

        } else {
            strcpy(array[j], relCache[relNum2].relationName);
            strcat(array[j], ".");
            strcat(array[j], temp2->attrName);
        }

        if (temp2->attrType == 0) {
            sprintf(val, "%s%d", "s", temp2->attrLength);
            strcpy(array[j + 1], val);
        } else if (temp2->attrType == 1) {
            strcpy(array[j + 1], "i");

        } else if (temp2->attrType == 2) {
            strcpy(array[j + 1], "f");
        }
        temp2 = temp2->next;
    }

    strcpy(array[j], "NIL");

    return array;
}

/*
 *
 * returns 1 if the join column values matches
 * otherwise 0
 *
 */
int matchJoinCol(int attrType, int outerAttrOffset, int innerAttrOffset, char *outerTupleData, char *innerTupleData,
                 int compOp) {

    int ival1, ival2;
    float fval1, fval2;

    switch (attrType) {
        case 0:
            return stringComparision(compOp, outerTupleData + outerAttrOffset, innerTupleData + innerAttrOffset);

        case 1:
            memcpy(&ival1, outerTupleData + outerAttrOffset, 4);
            memcpy(&ival2, innerTupleData + innerAttrOffset, 4);
            return integerValueComparision(compOp, ival1, ival2);

        case 2:
            memcpy(&fval1, outerTupleData + outerAttrOffset, 4);
            memcpy(&fval2, innerTupleData + innerAttrOffset, 4);
            return floatValueComparision(compOp, fval1, fval2);

        default:;
    }
}

/*------------------------------------------------------------

FUNCTION int Join(int argc, char *argv[])

PARAMETER DESCRIPTION:

 *
 * argv[0]    : "join"
 * argv[1]    : result relation
 * argv[2]    : source relation 1
 * argv[3]    : attribute name 1
 * argv[4]    : source relation 2
 * argv[5]    : attribute name 2
 * argv[argc] : NIL

SPECIFICATIONS:
       None.

ERRORS REPORTED:
        *      DB_NOT_OPEN
        *      DESTINATION_RELATION_EXIST
        *      SOURCE_RELATION_NOT_EXIST
        *      JOIN_COLUMN_NAME_MISMATCH.
        *      JOIN_COL_MISMATCH_SCHMA_COL

GLOBAL VARIABLES MODIFIED:
       "isSelfJoin" is set to 1 when self join is identified and reset to 0 again after self join completes.
       Setting this var will avoid "unique tuple error" printing in "InsertRec" function during self join.
       "isSelfJoin" is also used in naming convention of resultant table attributes.

IMPLEMENTATION NOTES (IF ANY):
        Does Relational Natural Join according to argv , argc, also does self join.
------------------------------------------------------------*/
int Join(int argc, char *argv[]) {

    if (isDBOpen() == NOTOK) {
        ErrorMsgs(DB_NOT_OPEN, 1);
        return NOTOK;
    }


    //create result relation first
    if (access(getFileName(argv[1]), F_OK) != -1) {
        ErrorMsgs(DESTINATION_RELATION_EXIST, 1);
        return NOTOK;
    } else if (access(getFileName(argv[2]), F_OK) == -1 || access(getFileName(argv[4]), F_OK) == -1) {
        ErrorMsgs(SOURCE_RELATION_NOT_EXIST, 1);
        return NOTOK;
    }

    // setting global var to 1 , if join is self join, using this value in Insertrec
    if (!(strcmp(argv[2], argv[4]))) {
        isSelfJoin = 1;
    } else {
        isSelfJoin = 0;
    }

    if (strcmp(argv[3], argv[5])) {
        ErrorMsgs(JOIN_COLUMN_NAME_MISMATCH, 1);
        return NOTOK;
    }

    int innerRelNo = FindRelNum(argv[2]);
    int outerRelNo = FindRelNum(argv[4]);

    int destRelNum, innerAttrType, outerAttrOffset;
    int innerAttrOffset, innerAttrSize, cnt;
    attrCatNode *destTravPtr, *outerTravPtr;
    char tempStr[MAX_RELNAME_SIZE], innerRelName[RELNAME_SIZE], outerRelName[RELNAME_SIZE];

    strcpy(innerRelName, argv[2]);
    strcpy(outerRelName, argv[4]);

    if (innerRelNo == NOTOK) {
        strcpy(innerRelName, argv[2]);
        innerRelNo = OpenRel(innerRelName);
    }

    if (outerRelNo == NOTOK) {
        strcpy(outerRelName, argv[4]);
        outerRelNo = OpenRel(outerRelName);
    }

    //   attrlist check
    if (getAttrSize(relCache[innerRelNo].attrList, argv[3]) == NOTOK ||
        getAttrSize(relCache[outerRelNo].attrList, argv[5]) == NOTOK) {
        ErrorMsgs(JOIN_COL_MISMATCH_SCHMA_COL, 1);
        return NOTOK;
    }

    // inner relation must have more tuples than outer tuple
    if (relCache[innerRelNo].pageCount < relCache[outerRelNo].pageCount) {
        // swapping if inner relation have less tuples  than outer
        innerRelNo = innerRelNo + outerRelNo;
        outerRelNo = innerRelNo - outerRelNo;
        innerRelNo = innerRelNo - outerRelNo;

        strcpy(innerRelName, argv[4]);
        strcpy(outerRelName, argv[2]);
    }

    int totalAttrCount = relCache[innerRelNo].attributeCount + relCache[outerRelNo].attributeCount;
    Create(((totalAttrCount - 1) * 2) + 2, getCreateArgvForJoin(argv[1], innerRelNo, outerRelNo, argv[3]));

    // opening join result relation
    OpenRel(argv[1]);
    destRelNum = FindRelNum(argv[1]);

    // creating join tuple data
    char *joinResultTuple = (char *) malloc((size_t) relCache[destRelNum].recordLength);
    char *outerTupleData = (char *) malloc((size_t) relCache[outerRelNo].recordLength);
    char *innerTupleData = (char *) malloc((size_t) relCache[innerRelNo].recordLength);

    Rid startRidOuter, foundRidOuter, startRidInner, foundRidInner;
    attrCatNode innerAttrList, outerAttrList;

    startRidOuter.pid = 0;
    startRidOuter.slotnum = 0;

    innerAttrType = validAttrNameAndGetAttrType(relCache[innerRelNo].attrList, argv[3]);
    outerAttrOffset = getattrOffset(outerRelNo, argv[3]);
    innerAttrOffset = getattrOffset(innerRelNo, argv[3]);
    innerAttrSize = getAttrSize(relCache[innerRelNo].attrList, argv[3]);


    while (GetNextRec(outerRelNo, &startRidOuter, &foundRidOuter, outerTupleData) == OK) {

        startRidInner.pid = 0;
        startRidInner.slotnum = 0;
        while (FindRec(innerRelNo, &startRidInner, &foundRidInner, innerTupleData, innerAttrType, innerAttrSize,
                       innerAttrOffset, outerTupleData, 501) == OK) {


            if (matchJoinCol(innerAttrType, outerAttrOffset, innerAttrOffset, outerTupleData, innerTupleData, 501) ==
                OK) {
                // matching tuple found

                // form the tuple
                memcpy(joinResultTuple, innerTupleData, (size_t) relCache[innerRelNo].recordLength);
                destTravPtr = relCache[destRelNum].attrList;

                cnt = 0;
                while (cnt < relCache[innerRelNo].attributeCount) {
                    cnt++;
                    destTravPtr = destTravPtr->next;
                }

                while (destTravPtr) {

                    outerTravPtr = relCache[outerRelNo].attrList;

                    if (isSelfJoin) {
                        while (outerTravPtr) {
                            strcpy(tempStr, "B");
                            strcat(tempStr, ".");
                            strcat(tempStr, outerTravPtr->attrName);
                            if (!strcmp(destTravPtr->attrName, tempStr))
                                break;

                            outerTravPtr = outerTravPtr->next;
                        }
                    } else {
                        while (outerTravPtr) {
                            strcpy(tempStr, outerRelName);
                            strcat(tempStr, ".");
                            strcat(tempStr, outerTravPtr->attrName);
                            if (!strcmp(destTravPtr->attrName, tempStr))
                                break;
                            outerTravPtr = outerTravPtr->next;
                        }
                    }

                    if (outerTravPtr)
                        memcpy(joinResultTuple + (destTravPtr->attrOffset), outerTupleData + (outerTravPtr->attrOffset),
                               (size_t) outerTravPtr->attrLength);

                    destTravPtr = destTravPtr->next;
                }

                InsertRec(destRelNum, joinResultTuple);
            }


            if (foundRidInner.slotnum == relCache[innerRelNo].recordsPerPage - 1 &&
                foundRidInner.pid + 1 < relCache[innerRelNo].pageCount) {
                // when foundRid is last slotNo and there exist next page
                startRidInner.slotnum = 0;
                startRidInner.pid = foundRidInner.pid + 1;
            } else if (foundRidInner.slotnum < relCache[innerRelNo].recordsPerPage - 1) {
                // when foundRid is not last slotNo and no need to check if there exist next page
                startRidInner.slotnum = foundRidInner.slotnum + 1;
                startRidInner.pid = foundRidInner.pid;
            } else {
                // when foundRid is last slotNo and there doesn't exist next page
                break;
            }

        }// inner while

        if (foundRidOuter.slotnum == relCache[outerRelNo].recordsPerPage - 1 &&
            foundRidOuter.pid + 1 < relCache[outerRelNo].pageCount) {
            // when foundRid is last slotNo and there exist next page
            startRidOuter.slotnum = 0;
            startRidOuter.pid = foundRidOuter.pid + 1;
        } else if (foundRidOuter.slotnum < relCache[outerRelNo].recordsPerPage - 1) {
            // when foundRid is not last slotNo and no need to check if there exist next page
            startRidOuter.slotnum = foundRidOuter.slotnum + 1;
            startRidOuter.pid = foundRidOuter.pid;
        } else {
            // when foundRid is last slotNo and there doesn't exist next page
            break;
        }
    }// outer while

    isSelfJoin = 0;
}