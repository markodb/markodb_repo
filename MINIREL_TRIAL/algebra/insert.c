#include "../include/defs.h"

/*------------------------------------------------------------

FUNCTION int Insert(int argc, char *argv[])

PARAMETER DESCRIPTION:
 * argv[0] - insert
 * argv[1] - relation name
 * argv[2] - attr name 1
 * argv[3] - attr value 1
 * ...
 * argv[argc] - NIL
 *
 * @param argc - Count of arguments
 * @param argv -
 * @return OK if success

ERRORS REPORTED:
      * DB_NOT_OPEN
      * USER_CANT_UPDATE_METADATA
      * INVALID_ATTR_LIST
      * INVALID_ATTR_COUNT

GLOBAL VARIABLES MODIFIED:
       None.

IMPLEMENTATION NOTES (IF ANY):
       This function implements relational Insert command. Checks for uniqueness of the tuple.

------------------------------------------------------------*/
int Insert(int argc, char *argv[]) {
    if (isDBOpen() == NOTOK) {
        ErrorMsgs(DB_NOT_OPEN, 1);
        return NOTOK;
    }

    int relNum;

    // finding relnum for given relation
    if (!strcmp(argv[1], RELCAT) || !strcmp(argv[1], ATTRCAT)) {
        ErrorMsgs(USER_CANT_UPDATE_METADATA, 1);
        return NOTOK;
    } else {
        relNum = OpenRel(argv[1]);
    }

    if(relNum == NOTOK){
        return NOTOK;
    }

    // validates the attributes
    if (validateAttributes(relNum, argc, argv) == NOTOK) {
        return NOTOK;
    }

    InsertRec(relNum, getRelationRecord(argc, argv, relNum));
    return OK;
}

/*
 *
 *  It validates all the attributes coming in INSERT argv
 *
 */
int validateAttributes(int relNum, int argc, char *argv[]) {

    int attrCount = (argc - 2) / 2;
    if (attrCount != relCache[relNum].attributeCount) {
        ErrorMsgs(INVALID_ATTR_COUNT, 1);
        return NOTOK;
    }

    attrCatNode *temp;
    temp = relCache[relNum].attrList;
    int validFlag = 0;
    while (temp) {
        for (int i = 2; i <= argc - 2; i = i + 2) {
            if (!strcmp(temp->attrName, argv[i]) && (validateAttrValues(temp->attrType, argv[i + 1]) == OK)) {
                validFlag = 1;
                break;
            }
        }

        if (validFlag == 0) {
            ErrorMsgs(INVALID_ATTR_LIST, 1);
            return NOTOK;
        }

        validFlag = 0;
        temp = temp->next;
    }

    return OK;
}


/*
 *
 * Uses insert argv argc and creates a relation record as BYTE ARRAY
 *
 */
char *getRelationRecord(int argc, char *argv[], int relNum) {
    char *recordPtr = (char *) malloc(relCache[relNum].recordLength);
    attrCatNode *temp = relCache[relNum].attrList;
    int attr_int;
    float attr_float;

    while (temp) {
        for (int i = 2; i <= argc - 2; i = i + 2) {
            if (!strcmp(temp->attrName, argv[i])) {

                if (temp->attrType == 0) {

                    memcpy(recordPtr + temp->attrOffset, argv[i + 1], temp->attrLength);
                } else if (temp->attrType == 1) {
                    attr_int = atoi(argv[i + 1]);
                    memcpy(recordPtr + temp->attrOffset, &attr_int, temp->attrLength);
                } else {
                    attr_float = (float) atof(argv[i + 1]);
                    memcpy(recordPtr + temp->attrOffset, &attr_float, temp->attrLength);
                }
                break;
            }
        }
        temp = temp->next;
    }

    return recordPtr;
}