#include "../include/defs.h"

/*
 *
 * "create" argv, for creation of resultant project table. This argv is used as input argument in
 * Create(int argc, char *argv[]).
 *
 */
char **getCreateArgvForProject(int argc, char *argv[], attrCatNode *temp) {
    char **array = (char **) malloc(ATTR_COUNT * sizeof(char *));
    int i, k;
    char val[5];
    int attrType;

    for (i = 0; i < ATTR_COUNT; i++)
        array[i] = (char *) malloc(MAX_ATTRNAME_SIZE);

    strcpy(array[0], "create");
    strcpy(array[1], argv[1]);

    for (k = 3, i = 0; k <= argc - 1; k++, i++) {
        strcpy(array[k - 1 + i], argv[k]);
        attrType = validAttrNameAndGetAttrType(temp, argv[k]);
        if (attrType == 0) {
            sprintf(val, "%s%d", "s", getAttrSize(temp, argv[k]));
            strcpy(array[k + i], val);
        } else if (attrType == 1) {
            strcpy(array[k + i], "i");

        } else if (attrType == 2) {
            strcpy(array[k + i], "f");

        }
    }

    strcpy(array[k - 1 + i], "NIL");

    return array;
}

/*
 *
 * returns destination record from SourceRecord . SourceRecord is returned by GetNextRec
 *
 */
char *getDestinationRecordFromSourceRecord(int destinationRelNum, char *sourceRecord, int sourceRelNum) {
    attrCatNode *destListHead = relCache[destinationRelNum].attrList;


    char *destRecord = (char *) malloc((size_t) relCache[destinationRelNum].recordLength);

    while (destListHead) {
        memcpy(destRecord + destListHead->attrOffset,
               sourceRecord + getattrOffset(sourceRelNum, destListHead->attrName), destListHead->attrLength);
        destListHead = destListHead->next;
    }
    return destRecord;
}

/*------------------------------------------------------------

FUNCTION int Project(int argc, char *argv[])
  * Implements the project command
 * Creates the result relation with the attributes given in argv,
 * then performs the projection

PARAMETER DESCRIPTION:
 *
 * argv[0] - "project"
 * argv[1] - result relation
 * argv[2] - source relation
 * argv[3] - attribute name 1
 * ....
 * argv[argc] - NIL

ERRORS REPORTED:
      * DB_NOT_OPEN
      * USER_CANT_UPDATE_METADATA
      * DESTINATION_RELATION_EXIST
      * SOURCE_RELATION_NOT_EXIST
      * ATTR_NAME_NOT_VALID

GLOBAL VARIABLES MODIFIED:
       isSelfJoin is modified .
       Setting this var will avoid "unique tuple error" printing in "InsertRec" function during Projection

IMPLEMENTATION NOTES (IF ANY):
       This function implements relational Project command. Checks for uniqueness of the tuple.

------------------------------------------------------------*/
int Project(int argc, char *argv[]) {

    if (isDBOpen() == NOTOK) {
        ErrorMsgs(DB_NOT_OPEN, 1);
        return NOTOK;
    }

    if (!strcmp(argv[2], RELCAT) || !strcmp(argv[2], ATTRCAT)) {
        ErrorMsgs(USER_CANT_OPEN_METADATA, 1);
        return NOTOK;
    }

    if (access(getFileName(argv[1]), F_OK) != -1) {
        ErrorMsgs(DESTINATION_RELATION_EXIST, 1);
        return NOTOK;
    } else if (access(getFileName(argv[2]), F_OK) == -1) {
        ErrorMsgs(SOURCE_RELATION_NOT_EXIST, 1);
        return NOTOK;
    }

    int sourceRelNum = FindRelNum(argv[2]), destinationRelNum;
    if (sourceRelNum == NOTOK) {
        sourceRelNum = OpenRel(argv[2]);
    }

    // all attributes should be valid
    for (int i = 3; i < argc; ++i) {
        if (getAttrSize(relCache[sourceRelNum].attrList, argv[i]) == NOTOK) {
            ErrorMsgs(ATTR_NAME_NOT_VALID, 1);
            return NOTOK;
        }
    }


    int destinationRelAttributeCount = argc - 3;
    Create(destinationRelAttributeCount * 2 + 2, getCreateArgvForProject(argc, argv, relCache[sourceRelNum].attrList));
    destinationRelNum = OpenRel(argv[1]);

    // inserting the records in Destination Relation
    Rid startRid, foundRid;
    startRid.pid = 0;
    startRid.slotnum = 0;

    char *foundRecordSource = (char *) malloc((size_t) relCache[sourceRelNum].recordLength);

    // to ignore error messages
    isSelfJoin = 1;
    while (1) {
        if (GetNextRec(sourceRelNum, &startRid, &foundRid, foundRecordSource) == OK) {

            InsertRec(destinationRelNum,
                      getDestinationRecordFromSourceRecord(destinationRelNum, foundRecordSource, sourceRelNum));

            if (foundRid.slotnum == relCache[sourceRelNum].recordsPerPage - 1 &&
                foundRid.pid + 1 < relCache[sourceRelNum].pageCount) {
                // when foundRid is last slotNo and there exist next page
                startRid.slotnum = 0;
                startRid.pid = foundRid.pid + 1;
            } else if (foundRid.slotnum < relCache[sourceRelNum].recordsPerPage - 1) {
                // when foundRid is not last slotNo and no need to check if there exist next page
                startRid.slotnum = foundRid.slotnum + 1;
                startRid.pid = foundRid.pid;
            } else {
                // when foundRid is last slotNo and there doesn't exist next page
                break;
            }

        } else {
            break;
        }

    }

    // again making error to print
    isSelfJoin = 0;

    return OK;
}




