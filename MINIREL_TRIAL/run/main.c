#include "../include/defs.h"

extern parser();

/*------------------------------------------------------------

FUNCTION main ()
 invokes parser
------------------------------------------------------------*/


int main() {
    initialize_database();
    printf(BLU "Welcome to MINIREL database system" RESET);
    parser();
}

