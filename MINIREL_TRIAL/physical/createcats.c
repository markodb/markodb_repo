/************************INCLUDES*******************************/
#include "../include/defs.h"
#include "../include/error.h"


/*------------------------------------------------------------

FUNCTION CreateCats()

IMPLEMENTATION NOTES (IF ANY):
       CreateCats() is invoked on "createdb" command calls two functions.
       CreateRelCat() statically creates relation entries for relcat & attributecat.
       CreateAttrCat() statically creates attribute entries for relcat & attributecat.

------------------------------------------------------------*/
int CreateCats() {
    if (CreateRelCat() == NOTOK || CreateAttrCat() == NOTOK) {
        // Setting printFlag = 1 because we want to print the error
        return ErrorMsgs(CATALOGS_CREATION_FAILED, 1);
    }
    return OK;
}


/*
 *
 * SlotMap perform three functions
 * SET_SLOT_BIT_1 : set bit at slotNumber to 1
 * SET_SLOT_BIT_0 : set bit at slotNumber to 0
 * GET_SLOT_BIT   : returns value of bit at slotNumber
 */
int SlotMap(char *PageBuffer, int slotNumber, int operation) {
    int slotByte;
    int slotBit;

    if (slotNumber > 7) {
        if ((slotNumber % 8) == 0) {
            slotByte = (slotNumber / 8);
            slotBit = 0;
        } else {
            slotByte = (slotNumber / 8);
            slotBit = slotNumber % 8;
        }
    } else {
        slotByte = 0;
        slotBit = slotNumber;
    }

    if (operation == SET_SLOT_BIT_1)
        PageBuffer[slotByte] |= (0x01 << (7 - slotBit));

    if (operation == SET_SLOT_BIT_0) {
        if (SlotMap(PageBuffer, slotNumber, GET_SLOT_BIT))
            PageBuffer[slotByte] ^= (0x01 << (7 - slotBit));
    }
    if (operation == GET_SLOT_BIT)
        return (PageBuffer[slotByte] & (0x01 << (7 - slotBit))) >> (7 - slotBit);
}


/*
 *
 * Statically creates relcat for newly created database.
 */
int CreateRelCat() {
    // checking if RelCat already exist
    if (access(getFileName(RELCAT), F_OK) != -1) {
        ErrorMsgs(RELCAT_ALREADY_EXIST, 1);
        return NOTOK;
    }

    //forming filename : /DBPATH/relCat
    FILE *file = fopen(getFileName(RELCAT), "a");

    char *PageBuffer = (char *) calloc(1, PAGESIZE);
    int relCatSlotMapSize = getSlotMapSize(getRelcatRecordLength());
    int totalRelcatAttributes = (getARGVcount(getRelcatAttributes()) - 2) / 2;
    int totalAttrcatAttributes = (getARGVcount(getAttrcatAttributes()) - 2) / 2;
    size_t byteWrittenCount = 0;

    //obj1
    relRecord relObj1;
    relObj1.attributeCount = totalRelcatAttributes;
    relObj1.pageCount = 1;
    relObj1.recordCount = 2;
    relObj1.recordsPerPage = getTotalSlots(getRelcatRecordLength());
    relObj1.recordLength = getRelcatRecordLength();
    strcpy(relObj1.relationName, RELCAT);

    //obj2
    relRecord relObj2;
    relObj2.attributeCount = totalAttrcatAttributes;
    relObj2.pageCount = 1;
    relObj2.recordCount = totalRelcatAttributes + totalAttrcatAttributes;
    relObj2.recordsPerPage = getTotalSlots(getAttrcatRecordLength());
    relObj2.recordLength = getAttrcatRecordLength();
    strcpy(relObj2.relationName, ATTRCAT);

    //placing relcat tuple in page buffer
    memcpy(PageBuffer + relCatSlotMapSize + 0 * sizeof(relObj1), (void *) &relObj1, getRelcatRecordLength());
    memcpy(PageBuffer + relCatSlotMapSize + 1 * sizeof(relObj2), (void *) &relObj2, getRelcatRecordLength());

    //setting slotMap for new relcat tuples
    SlotMap(PageBuffer, 0, SET_SLOT_BIT_1);
    SlotMap(PageBuffer, 1, SET_SLOT_BIT_1);

    //writing to file
    byteWrittenCount = fwrite(PageBuffer, 1, PAGESIZE, file);

    //freeing resources
    free(PageBuffer);
    fclose(file);

    if (byteWrittenCount < PAGESIZE)
        return NOTOK;

    return OK;
}


/*
 *
 * Statically creates attrcat for newly created database.
 */
int CreateAttrCat() {
    // checking if AttrCat already exist
    if (access(getFileName(ATTRCAT), F_OK) != -1) {
        ErrorMsgs(ATTRCAT_ALREADY_EXIST, 1);
        return NOTOK;
    }

    FILE *file = fopen(getFileName(ATTRCAT), "a");

    char *PageBuffer = (char *) calloc(1, PAGESIZE);
    int i, attrCatSlotMapSize = getSlotMapSize(getAttrcatRecordLength());
    size_t byteWrittenCount = 0;

    //writing relcat attributes
    char **argv = getRelcatAttributes();
    int relCatAttributeCount = (getARGVcount(argv) - 2) / 2;

    attrRecord *attrRecord1 = setAttrFields(argv);
    memcpy(PageBuffer + attrCatSlotMapSize, (void *) attrRecord1, sizeof(attrRecord) * relCatAttributeCount);

    for (i = 0; i < relCatAttributeCount; i++)
        SlotMap(PageBuffer, i, SET_SLOT_BIT_1);


    //writing attr attributes
    argv = getAttrcatAttributes();
    int attrCatAttributeCount = (getARGVcount(argv) - 2) / 2;

    attrRecord1 = setAttrFields(argv);
    memcpy(PageBuffer + attrCatSlotMapSize + relCatAttributeCount * sizeof(attrRecord), (void *) attrRecord1,
           sizeof(attrRecord) * attrCatAttributeCount);

    for (i = 0; i < attrCatAttributeCount; i++)
        SlotMap(PageBuffer, i + relCatAttributeCount, SET_SLOT_BIT_1);

    byteWrittenCount = fwrite(PageBuffer, 1, PAGESIZE, file);

    free(PageBuffer);
    free(attrRecord1);
    fclose(file);

    if (byteWrittenCount < PAGESIZE)
        return NOTOK;

    return OK;
}