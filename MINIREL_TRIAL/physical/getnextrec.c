#include "../include/defs.h"

/*------------------------------------------------------------

FUNCTION GetNextRec(int relNum, Rid *startRid, Rid *foundRid, char *recPtr)

PARAMETER DESCRIPTION:
       relNum   : slot number in relCache
       startRid : page number & slot number of record, from where to search
                  for next record.
       foundRid : page number & slot number of next record
       recPtr   : pointer to character array where found record is returned

IMPLEMENTATION NOTES (IF ANY):
       This function get the next record after startRid slot,
       found record data is returned in *recPtr

------------------------------------------------------------*/

int GetNextRec(int relNum, Rid *startRid, Rid *foundRid, char *recPtr) {

    int i, startSlotIndex;

    // if current buffered page is different from required page
    if (pageBuffer[relNum].pid != startRid->pid) {
        FlushPage(relNum);
        ReadPage(relNum, startRid->pid);
    }

    //start index, after which searching must be done
    startSlotIndex = startRid->slotnum;

    //for each page
    do {

        // finding next occupied slot in current page
        for (i = startSlotIndex; i < relCache[relNum].recordsPerPage; i++) {
            if (SlotMap(pageBuffer[relNum].pageData, i, GET_SLOT_BIT) == 1) {

                foundRid->slotnum = i;
                foundRid->pid = pageBuffer[relNum].pid;

                memcpy(recPtr, (pageBuffer[relNum].pageData) + relCache[relNum].slotBitmapSize +
                               i * relCache[relNum].recordLength, (size_t) relCache[relNum].recordLength);

                return OK;      //occupied slot found
            }
        }

        // if next page exist, load next page to find next record in that
        if (((startRid->pid) + 1 < relCache[relNum].pageCount)) {
            FlushPage(relNum);
            ReadPage(relNum, pageBuffer[relNum].pid + 1);     // load next page, to what is loaded now
            startSlotIndex = 0;
        } else {
            // no next page exist and next record not found
            recPtr = NULL;
            return NOTOK;
        }
    } while (1);

}