#include "../include/defs.h"

/*------------------------------------------------------------

FUNCTION FindRelNum(char *relName)

PARAMETER DESCRIPTION:
       relName : relation name

IMPLEMENTATION NOTES (IF ANY):
       Returns relNo of relation if it is currently opened.
       relNo is searched on relName.

------------------------------------------------------------*/
int FindRelNum(char *relName) {
    int relNum = 0;

    for (relNum = 2; relNum < MAXOPEN; relNum++) {
        if (pageValid[relNum] == 1 && !strcmp(relName, relCache[relNum].relationName))
            return relNum;
    }
    return NOTOK;
}
