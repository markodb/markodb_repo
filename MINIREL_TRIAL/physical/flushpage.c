#include "../include/defs.h"

/*------------------------------------------------------------

FUNCTION FlushPage(int relNum)

PARAMETER DESCRIPTION:
       relNum : index number in relCache

IMPLEMENTATION NOTES (IF ANY):
       This function flushes Data buffer to disk file for
       corresponding relNum relation

------------------------------------------------------------*/
void FlushPage(int relNum) {
    if (pageBuffer[relNum].dirty == 0) {
        return;
    }

    FILE *file = relCache[relNum].filePtr;

    if (file == NULL) {
        ErrorMsgs(UNABLE_TO_FLUSH_PAGE, 1);
    }

    //seek file pointer to correct page number for writing
    fseek(file, pageBuffer[relNum].pid * PAGESIZE, SEEK_SET);

    fwrite(pageBuffer[relNum].pageData, 1, PAGESIZE, file);
    pageBuffer[relNum].dirty = 0;

    fflush(file);
}
