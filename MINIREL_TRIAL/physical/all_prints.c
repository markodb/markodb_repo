#include "../include/defs.h"

/*
 *
 * Convert string to float
 */
float stof(char *s) {
    float rez = 0, fact = 1;
    if (*s == '-') {
        s++;
        fact = -1;
    };
    for (int point_seen = 0; *s; s++) {
        if (*s == '.') {
            point_seen = 1;
            continue;
        };
        int d = *s - '0';
        if (d >= 0 && d <= 9) {
            if (point_seen) fact /= 10.0f;
            rez = rez * 10.0f + (float) d;
        };
    };
    return rez * fact;
};


/*
 *
 * HELPER : Print relcache entries
 */
void print_relcacheEntries(int relNum) {

    int count = 1;
    printf("\n%s\n", "************  CACHE DATA  ************");

    printf("\nrelationName    : %s", relCache[relNum].relationName);
    printf("\nrecordLength    : %d", relCache[relNum].recordLength);
    printf("\nrecordsPerPage  : %d", relCache[relNum].recordsPerPage);
    printf("\nattributeCount  : %d", relCache[relNum].attributeCount);
    printf("\nrecordCount     : %d", relCache[relNum].recordCount);
    printf("\npageCount       : %d", relCache[relNum].pageCount);
    printf("\nslotbitmapSize  : %d", relCache[relNum].slotBitmapSize);
    printf("\ntime            : (%d : %d)", relCache[relNum].time.tv_sec, relCache[relNum].time.tv_usec);
    printf("\npageNo - slotNo :  %d - %d", relCache[relNum].relcatRid.pid, relCache[relNum].relcatRid.slotnum);
    printf("\ndirty           : %d", relCache[relNum].dirty);
    printf("\n%s", "ATTRIBUTE LIST");

    attrCatNode *temp = relCache[relNum].attrList;

    while (temp) {
        printf("\n[+] Attribute %d -------------", count++);
        printf("\nattrName : %s", temp->attrName);
        printf("\ntype     : %d", temp->attrType);
        printf("\noffset   : %d", temp->attrOffset);
        printf("\nlength   : %d", temp->attrLength);
        temp = temp->next;
    }

}


/*
 *
 * HELPER : Print Page buffer live data
 */
void print_buffer_page(int relNum) {
    printf("\n\t%s\n", "************  PAGE DATA  ************");

    char *temp, *trav, temp_str_output[25], int_string[12];
    relRecord *obj1;

    temp_str_output[0] = '\0';

    attrCatNode *list = relCache[relNum].attrList;
    int count = 0, i = 0;

    //temp pointing buffer now
    temp = pageBuffer[relNum].pageData;

    slotMap_printing(temp, relCache[relNum].slotBitmapSize);
    //temp pointing actual data now
    temp = temp + getSlotMapSize(relCache[relNum].recordLength);

    printf("\n\n");
    while (list) {
        printf("%-20s", list->attrName);
        list = list->next;
    }

    while (count < relCache[relNum].recordsPerPage) {

        list = relCache[relNum].attrList;
        printf("\n");
        if (SlotMap(pageBuffer[relNum].pageData, i, GET_SLOT_BIT)) {
            while (list) {

                printf("%-20s ", getVal(temp, list));
                list = list->next;
            }
            i++;
        } else {
            printf("%s", "EMPTY ROW");
            i++;
        }

        printf("\n");
        count++;
        temp = temp + relCache[relNum].recordLength;
    }


}


/*
 *
 * HELPER : Prints pageValid Status
 */
void print_slot_status(int relNum) {
    printf("\n%s\n", "************  STATUS BIT  ************");
    printf("Valid Bit : %d", pageValid[relNum]);
}


/*
 *
 * HELPER : Prints Page buffer,Relcache entry, pageValid Bit of given relNumber
 */
int all_prints(int relNum) {
    print_buffer_page(relNum);
    print_relcacheEntries(relNum);
    print_slot_status(relNum);
}


/*
 *
 * Convert byte sequence to int/float/string
 * depending on attrType in list node
 */
char *getVal(char *temp, attrCatNode *list) {

    static char final_output[50], tempval[4];
    tempval[3] = '\0';
    int a = 0;
    float b = 0.0;
    char *ptr;

    switch (list->attrType) {

        case 0: //STRING
            ptr = temp + (list->attrOffset);
            memcpy(&final_output, (void *) ptr, (size_t) list->attrLength);
            final_output[list->attrLength] = '\0';
            return final_output;

        case 1: //INTEGER
            ptr = temp + (list->attrOffset);
            memcpy(&a, ptr, 4);
            snprintf(final_output, 25, "%d", a);
            return final_output;

        case 2: //FLOAT
            ptr = temp + (list->attrOffset);
            memcpy(&b, ptr, 4);
            snprintf(final_output, 25, "%.2f", b);
            return final_output;
    }

}


/*
 *
 * Print whole tuple(inputted as raw Byte Array)
 * as per Attribute list of relation
 */
void print_tuple(int relNum, char *temp) {

    attrCatNode *list = relCache[relNum].attrList;
    printf("\n");
    while (list) {
        printf("%-18s", getVal(temp, list));
        list = list->next;
    }
}
