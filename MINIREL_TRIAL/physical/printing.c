#include "../include/defs.h"

int total_record_relcat = 1, total_record_attrcat = 1;

/*
 *
 * HELPER : Prints relCat, reading from relCat file.
 *          Prints all rows without slotMap lookup
 */
void relcat_printing() {

    char fileName[100];
    strcpy(fileName, currOpenDB);
    strcat(fileName, "/");
    strcat(fileName, RELCAT);
    FILE *file = fopen(fileName, "r");
    relRecord *ptr;
    int count = 0, record_cnt = 0;
    int relCatSlotMapSize = getSlotMapSize(getRelcatRecordLength());

    ptr = (relRecord *) malloc(sizeof(relRecord));
    fseek(file, relCatSlotMapSize, SEEK_SET);

    printf("\n\n%s",
           "*******************************      RELCAT     *************************************************");
    printf("\n%-15s %-15s %-15s %-15s %-15s %-15s", "relationName", "attributeCount", "pageCount", "recordCount",
           "recordsPerPage", "recordLength");
    printf("\n-------------------------------------------------------------------------------------------------");

    while ((count = fread(ptr, 1, sizeof(relRecord), file)) > 0 && record_cnt < total_record_relcat) {
        if (record_cnt == 0)
            total_record_relcat = ptr->recordCount;
        if (record_cnt == 1)
            total_record_attrcat = ptr->recordCount;

        printf("\n%-15s %-15d %-15d %-15d %-15d %-15d", ptr->relationName, ptr->attributeCount, ptr->pageCount,
               ptr->recordCount, ptr->recordsPerPage, ptr->recordLength);
        record_cnt++;
    }
}


/*
 *
 * HELPER : Prints attrCat, reading from attrCat file.
 *          Prints all rows without slotMap lookup
 */
void attrcat_printing() {

    char fileName[100];
    strcpy(fileName, currOpenDB);
    strcat(fileName, "/");
    strcat(fileName, ATTRCAT);
    FILE *file = fopen(fileName, "r");

    attrRecord *ptr;
    int count = 0, record_cnt = 0;
    int attrCatSlotMapSize = getSlotMapSize(getAttrcatRecordLength());

    ptr = (attrRecord *) malloc(sizeof(attrRecord));
    fseek(file, attrCatSlotMapSize, SEEK_SET);

    printf("\n\n%s",
           "*******************************      ATTRCAT     *************************************************");
    printf("\n%-15s %-15s %-15s %-15s %-15s", "relationName", "attrName", "attrType", "attrLength", "attrOffset");
    printf("\n-------------------------------------------------------------------------------------------------");


    while ((count = fread(ptr, 1, sizeof(attrRecord), file)) > 0 && record_cnt < total_record_attrcat) {
        printf("\n%-15s %-15s %-15d %-15d %-15d", ptr->relationName, ptr->attrName, ptr->attrType, ptr->attrLength,
               ptr->attrOffset);
        record_cnt++;
    }
}


/*
 *
 * HELPER : Prints slotMap bits of a page.
 */
void slotMap_printing(char *Page, int slotMapSizeInBytes) {
    int count = 0, i = 0, bitcount = 0;
    while (count < slotMapSizeInBytes) {
        printf("\n");
        for (i = 7; i >= 0; i--) {
            printf("%3d(%d) ", bitcount++, (Page[count] & (0x01 << i)) >> i);
        }
        count++;
    }
}