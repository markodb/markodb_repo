#include "../include/defs.h"

/*------------------------------------------------------------

FUNCTION WriteRec(int relNum, char *recPtr, Rid recRid)

PARAMETER DESCRIPTION:
       relNum   : slot number in relCache
       recPtr   : pointer to character array where found record is returned
       recRid   : destination page Number & slot Number of record to be written

IMPLEMENTATION NOTES (IF ANY):
       This function brings the desired page to buffer and
       writes the record in given slot number in that page.

------------------------------------------------------------*/
void WriteRec(int relNum, char *recPtr, Rid recRid) {

    // if required page is not present in buffer, bring that
    if (pageBuffer[relNum].pid != recRid.pid) {
        FlushPage(relNum);
        ReadPage(relNum, recRid.pid);
    }

    memcpy(pageBuffer[relNum].pageData +
           (relCache[relNum].slotBitmapSize + (recRid.slotnum * relCache[relNum].recordLength)), recPtr,
           (size_t) relCache[relNum].recordLength);

    // Page is dirty after writing
    pageBuffer[relNum].dirty = 1;
    updateTimestamp(relNum);
}
