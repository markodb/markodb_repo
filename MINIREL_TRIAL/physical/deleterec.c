#include "../include/defs.h"

/*------------------------------------------------------------

FUNCTION DeleteRec(int relNum, Rid *recRid)

PARAMETER DESCRIPTION:
       relNum : slot number in relCache
       recRid : page number & slot number of record to be deleted

IMPLEMENTATION NOTES (IF ANY):
       Deletes the record pointed by recRid.
       PageCount won't be decremented, even if page becomes empty
       by multiple DeleteRec.

------------------------------------------------------------*/
void DeleteRec(int relNum, Rid *recRid) {
    if (pageBuffer[relNum].pid != recRid->pid) {
        FlushPage(relNum);
        ReadPage(relNum, recRid->pid);
    }

    SlotMap(pageBuffer[relNum].pageData, recRid->slotnum, SET_SLOT_BIT_0);
    pageBuffer[relNum].dirty = 1;

    relCache[relNum].recordCount -= 1;
    relCache[relNum].dirty = 1;

    FlushPage(relNum);

    //update timestamp, used for LRU replacement policy
    updateTimestamp(relNum);
}
