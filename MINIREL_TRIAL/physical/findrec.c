#include "../include/defs.h"


/*
 * function for comparing two integers
 */
int integerValueComparision(int compOp, int value1, int value2) {

    switch (compOp) {

        case 501:
            if (value1 == value2) return OK;
            return NOTOK;

        case 502:
            if (value1 >= value2) return OK;
            return NOTOK;

        case 503:
            if (value1 > value2) return OK;
            return NOTOK;

        case 504:
            if (value1 <= value2) return OK;
            return NOTOK;

        case 505:
            if (value1 != value2) return OK;
            return NOTOK;

        case 506:
            if (value1 < value2) return OK;
            return NOTOK;

        default:
            printf("\n%s", "Operator not found");
            return NOTOK;
    }
}


/*
 * function for comparing two floats
 */
int floatValueComparision(int compOp, double value1, double value2) {

    switch (compOp) {

        case 501:
            if (value1 == value2) return OK;
            return NOTOK;

        case 502:
            if (value1 >= value2) return OK;
            return NOTOK;

        case 503:
            if (value1 > value2) return OK;
            return NOTOK;

        case 504:
            if (value1 <= value2) return OK;
            return NOTOK;

        case 505:
            if (value1 != value2) return OK;
            return NOTOK;

        case 506:
            if (value1 < value2) return OK;
            return NOTOK;

        default:
            printf("\n%s", "Operator not found");
            return NOTOK;
    }
}


/*
 * function for comparing two strings
 */
int stringComparision(int compOp, char *str1, char *str2) {

    switch (compOp) {

        case 501:
            if (!strcmp(str1, str2)) return OK;
            return NOTOK;

        case 502:
            if (strcmp(str1, str2) > 0 || !strcmp(str1, str2)) return OK;
            return NOTOK;

        case 503:
            if (strcmp(str1, str2) > 0) return OK;
            return NOTOK;

        case 504:
            if (strcmp(str1, str2) < 0 || !strcmp(str1, str2)) return OK;
            return NOTOK;

        case 505:
            if (strcmp(str1, str2) > 0 || strcmp(str1, str2) < 0) return OK;
            return NOTOK;

        case 506:
            if (strcmp(str1, str2) < 0) return OK;
            return NOTOK;

        default:
            printf("\n%s", "Operator not found");
            return NOTOK;
    }
}


/*
 * function to decide whether or not, a attribute from two tuples match or not
 */
int matchTuple(int attrType, int attrSize, int attrOffset, int compOp, char *fetchedTuple, char *searchTuple) {

    int ival1, ival2;
    float fval1, fval2;

    switch (attrType) {
        case 0:
            return stringComparision(compOp, fetchedTuple + attrOffset, searchTuple + attrOffset);

        case 1:
            memcpy(&ival1, fetchedTuple + attrOffset, 4);
            memcpy(&ival2, searchTuple + attrOffset, 4);
            return integerValueComparision(compOp, ival1, ival2);

        case 2:
            memcpy(&fval1, fetchedTuple + attrOffset, 4);
            memcpy(&fval2, searchTuple + attrOffset, 4);
            return floatValueComparision(compOp, fval1, fval2);

        default:;
    }
}


/*
 * function to find a record with given attribute value
 */
int FindRec(int relNum, Rid *startRid, Rid *foundRid, char *recPtr, int attrType, int attrSize, int attrOffset,
            char *searchTuple, int compOp) {

    char *fetchedTuple = (char *) malloc((size_t) relCache[relNum].recordLength);

    do {

        // if no next record exist
        if (GetNextRec(relNum, startRid, foundRid, fetchedTuple) == NOTOK) {
            free(fetchedTuple);
            return NOTOK;
        }

        // if tuple is matched
        if (matchTuple(attrType, attrSize, attrOffset, compOp, fetchedTuple, searchTuple) == OK) {

            memcpy(recPtr, fetchedTuple, (size_t) relCache[relNum].recordLength);
            free(fetchedTuple);
            return OK;
        } else {

            // getNext REC on next RID

            if (foundRid->slotnum == relCache[relNum].recordsPerPage - 1 &&
                foundRid->pid + 1 < relCache[relNum].pageCount) {
                // when foundRid is last slotNo and there exist next page
                startRid->slotnum = 0;
                startRid->pid = foundRid->pid + 1;
            } else if (foundRid->slotnum < relCache[relNum].recordsPerPage - 1) {
                // when foundRid is not last slotNo and no need to check if there exist next page
                startRid->slotnum = foundRid->slotnum + 1;
                startRid->pid = foundRid->pid;
            } else {
                // when foundRid is last slotNo and there doesn't exist next page
                break;
            }
        }
    } while (1);

    return NOTOK;
}