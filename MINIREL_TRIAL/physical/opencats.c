#include "../include/defs.h"

/*
 *
 * Statically create attribute list from attrCat, for relCat relation
 * and attrCat relation.
 */
attrCatNode *getAttributeListForCache(char *relName) {

    attrRecord object;
    attrCatNode *head = NULL, *temp;
    int attrcatRecordLength = getAttrcatRecordLength();
    int i = 0;

    while (i <= getTotalSlots(attrcatRecordLength)) {

        memcpy(&object, (pageBuffer[1].pageData) + getSlotMapSize(attrcatRecordLength) + i * attrcatRecordLength,
               attrcatRecordLength);

        if (!strcmp(object.relationName, relName)) {
            attrCatNode *newnode = (attrCatNode *) malloc(sizeof(attrCatNode));
            newnode->next = NULL;

            newnode->attrLength = object.attrLength;
            newnode->attrOffset = object.attrOffset;
            newnode->attrType = object.attrType;
            strcpy(newnode->attrName, object.attrName);

            if (head == NULL) {
                head = newnode;
            } else {
                temp = head;
                while (temp->next != NULL)
                    temp = temp->next;

                temp->next = newnode;
            }
        }
        i++;
    }
    return head;
}


/*
 *
 * Dynamically create attribute list from attrCat, for all other relations.
 *
 */
attrCatNode *getAttributeList(char *relName) {

    attrCatNode *head = NULL, *temp;

    Rid startRid, foundRid;
    startRid.pid = 0;
    startRid.slotnum = 0;
    int attrType = 0, compOp = 501;
    int attrSize = MAX_RELNAME_SIZE;
    int attrOffset = getattrOffset(1, "relationName");

    attrRecord sourceRecord, foundRecord;
    strcpy(sourceRecord.relationName, relName);


    while (1) {
        if (FindRec(1, &startRid, &foundRid, (char *) &foundRecord, attrType, attrSize, attrOffset,
                    (char *) &sourceRecord, compOp) == OK) {
            attrCatNode *newnode = (attrCatNode *) malloc(sizeof(attrCatNode));
            newnode->next = NULL;

            newnode->attrLength = foundRecord.attrLength;
            newnode->attrOffset = foundRecord.attrOffset;
            newnode->attrType = foundRecord.attrType;
            strcpy(newnode->attrName, foundRecord.attrName);

            if (head == NULL) {
                head = newnode;
            } else {
                temp = head;
                while (temp->next != NULL)
                    temp = temp->next;

                temp->next = newnode;
            }

            if (foundRid.slotnum == relCache[1].recordsPerPage - 1 && foundRid.pid + 1 < relCache[1].pageCount) {
                // when foundRid is last slotNo and there exist next page
                startRid.slotnum = 0;
                startRid.pid = foundRid.pid + 1;
            } else if (foundRid.slotnum < relCache[1].recordsPerPage - 1) {
                // when foundRid is not last slotNo and no need to check if
                // there exist next page
                startRid.slotnum = foundRid.slotnum + 1;
                startRid.pid = foundRid.pid;
            } else {
                // when foundRid is last slotNo and there doesn't exist next page
                break;
            }

        } else {
            break;
        }

    }
    return head;
}


/*
 *
 * Load relCache entries for relCat and attrCat
 */
int loadCatsToRelcache() {

    relRecord relobj, attrobj;
    memcpy(&relobj, (pageBuffer[0].pageData) + getSlotMapSize(sizeof(relRecord)), sizeof(relRecord));
    memcpy(&attrobj, (pageBuffer[0].pageData) + getSlotMapSize(sizeof(relRecord)) + sizeof(relRecord),
           sizeof(relRecord));

    // making relcache entry for RELCAT table at index 0
    strcpy(relCache[0].relationName, relobj.relationName);
    relCache[0].recordLength = relobj.recordLength;
    relCache[0].recordsPerPage = relobj.recordsPerPage;
    relCache[0].attributeCount = relobj.attributeCount;
    relCache[0].recordCount = relobj.recordCount;
    relCache[0].pageCount = relobj.pageCount;
    relCache[0].slotBitmapSize = getSlotMapSize(getRelcatRecordLength());
    relCache[0].relcatRid.pid = 0;
    relCache[0].relcatRid.slotnum = 0;
    relCache[0].filePtr = fopen(getFileName(RELCAT), "r+");
    relCache[0].dirty = 0;
    relCache[0].attrList = getAttributeListForCache(RELCAT);
    updateTimestamp(0);

    // making relcache entry for ATTRCAT table at index 1
    strcpy(relCache[1].relationName, attrobj.relationName);
    relCache[1].recordLength = attrobj.recordLength;
    relCache[1].recordsPerPage = attrobj.recordsPerPage;
    relCache[1].attributeCount = attrobj.attributeCount;
    relCache[1].recordCount = attrobj.recordCount;
    relCache[1].pageCount = attrobj.pageCount;
    relCache[1].slotBitmapSize = getSlotMapSize(getAttrcatRecordLength());
    relCache[1].relcatRid.pid = 0;
    relCache[1].relcatRid.slotnum = 1;
    relCache[1].filePtr = fopen(getFileName(ATTRCAT), "r+");
    relCache[1].dirty = 0;
    relCache[1].attrList = getAttributeListForCache(ATTRCAT);
    updateTimestamp(1);

    // relcat and attrcat are both loaded now
    pageValid[0] = 1;
    pageValid[1] = 1;
}


/*------------------------------------------------------------

FUNCTION OpenCats()

IMPLEMENTATION NOTES (IF ANY):
       This function reads page 0 of both relcat & attrcat.
       Simultaneously, relCache entries are also loaded for both.

------------------------------------------------------------*/
void OpenCats() {

    // loading Page no 0 of both RELCAT and ATTRCAT into PAGEBUFFER[0], PAGEBUFFER[1] respectively
    ReadPage(0, 0);
    ReadPage(1, 0);

    loadCatsToRelcache();
}
