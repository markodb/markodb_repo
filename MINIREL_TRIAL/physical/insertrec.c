#include "../include/defs.h"


/*
 *
 *  isTupleMatching function matches each attribute of searchTuple with examineTuple
 */

int isTupleMatching(char *searchTuple, char *examineTuple, int relNum) {

    int count = 0;
    attrCatNode *temp = relCache[relNum].attrList;

    while (temp) {
        if (matchTuple(temp->attrType, temp->attrLength, temp->attrOffset, 501, examineTuple, searchTuple) == OK)
            count++;
        temp = temp->next;
    }

    if (count == relCache[relNum].attributeCount)
        return OK;      // all attributes matched, tuple already exist

    return NOTOK;       // atleast one attribute don't match
}


/*
 *
 *  tupleExist function searches for searchTuple in relation corresponding to relNum
 */

int tupleExist(int relNum, char *searchTuple) {

    Rid startRid, foundRid;
    char *examineTuple = (char *) malloc((size_t) relCache[relNum].recordLength);

    startRid.pid = 0;
    startRid.slotnum = 0;

    while (GetNextRec(relNum, &startRid, &foundRid, examineTuple) == OK) {

        if (isTupleMatching(searchTuple, examineTuple, relNum) == OK) {
            free(examineTuple);
            return OK;          // matching tuple found
        }

        if (foundRid.slotnum == relCache[relNum].recordCount - 1 && foundRid.pid + 1 < relCache[relNum].pageCount) {
            // when foundRid is last slotNo and there exist next page
            startRid.slotnum = 0;
            startRid.pid = foundRid.pid + 1;
        } else if (foundRid.slotnum < relCache[relNum].recordCount - 1) {
            // when foundRid is not last slotNo and no need to check
            // if there exist next page
            startRid.slotnum = foundRid.slotnum + 1;
            startRid.pid = foundRid.pid;
        } else {
            // when foundRid is last slotNo and there doesn't exist next page
            break;
        }
    }

    free(examineTuple);
    return NOTOK;               // matching tuple not found
}


/*------------------------------------------------------------

FUNCTION InsertRec(int relNum, char *recPtr)

PARAMETER DESCRIPTION:
       relNum   : slot number in relCache
       recPtr   : pointer to character array where found record is returned

IMPLEMENTATION NOTES (IF ANY):
       InsertRec Inserts a given record, in relation corresponding to relNum

------------------------------------------------------------*/

void InsertRec(int relNum, char *recPtr) {
    int i = 0, emptyIndex = -1, pageNo = 0, val;
    Rid recRid;

    // checking uniqueness,
    // if tuple already exist, can not insert


    if (tupleExist(relNum, recPtr) == OK) {
        ErrorMsgs(TUPLE_ALREADY_EXIST, !isSelfJoin);
        return;
    }


    // if tuple is unique, then insert it
    // check for empty slot in current page
    for (i = 0; i < relCache[relNum].recordsPerPage; i++) {
        if (!SlotMap(pageBuffer[relNum].pageData, i, GET_SLOT_BIT)) {
            emptyIndex = i;
            break;
        }
    }

    // if no slot in current page, check for slot from beginning in all pages
    while (pageNo < relCache[relNum].pageCount && emptyIndex == -1) {

        // reading new pages
        FlushPage(relNum);
        ReadPage(relNum, pageNo);
        pageNo++;

        // finding empty slot in each page
        for (i = 0; i < relCache[relNum].recordsPerPage; i++) {
            if (!SlotMap(pageBuffer[relNum].pageData, i, GET_SLOT_BIT)) {
                emptyIndex = i;
                break;
            }
        }
    }

    // if slot found then insert in that else, if no slot found in all pages,
    // add a new page
    if (emptyIndex != -1) {

        recRid.pid = pageBuffer[relNum].pid;
        recRid.slotnum = emptyIndex;
        WriteRec(relNum, recPtr, recRid);

        // update slotmap
        SlotMap(pageBuffer[relNum].pageData, emptyIndex, SET_SLOT_BIT_1);

        // update relcache entries
        relCache[relNum].recordCount += 1;
        relCache[relNum].dirty = 1;

    } else {

        // if all existing pages are completely filled,
        // add a new page at end and insert new record in that

        // flush currently available page in buffer
        FlushPage(relNum);

        // creating New page in File using same page buffer
        pageBuffer[relNum].dirty = 1;
        pageBuffer[relNum].pid = pageBuffer[relNum].pid + 1;
        FlushPage(relNum);
        relCache[relNum].pageCount += 1;          // updating total page count
        relCache[relNum].dirty = 1;               // due to update in page count

        // clear all slotmap in new page
        for (i = 0; i < relCache[relNum].recordsPerPage; i++)
            SlotMap(pageBuffer[relNum].pageData, i, SET_SLOT_BIT_0);

        // writing new record in new page
        recRid.pid = pageBuffer[relNum].pid;
        recRid.slotnum = 0;
        WriteRec(relNum, recPtr, recRid);

        // update slotmap for first record
        SlotMap(pageBuffer[relNum].pageData, 0, SET_SLOT_BIT_1);
        relCache[relNum].recordCount += 1;
        pageBuffer[relNum].dirty = 1;

        FlushPage(relNum);
    }

}