#include "../include/defs.h"

/*------------------------------------------------------------

FUNCTION OpenRel(char *relName)

PARAMETER DESCRIPTION:
       relName   : Name of relation

ERRORS REPORTED:
       USER_CANT_OPEN_METADATA
       REL_NOEXIST


IMPLEMENTATION NOTES (IF ANY):
       Open the relation whose relName is passed.
       Returns slot Number of relCache where relation is opened.

------------------------------------------------------------*/
int OpenRel(char *relName) {
    int relNum = FindRelNum(relName);
    if (relNum != NOTOK) {
        return relNum;
    }
    if (access(getFileName(relName), F_OK) != -1) {

        // openRel shoudnot try to open RELCAT and AttrCat into memory buffers
        if (!strcmp(relName, RELCAT) || !strcmp(relName, ATTRCAT)) {
            ErrorMsgs(USER_CANT_OPEN_METADATA, 1);
            return NOTOK;
        }
        // file exists
        relNum = getEmptyBufferSlot();

        // to create Relcatcache record for this relnum, then use filePtr to update pageBuffer[relNum]
        CreateCacheEntryForRelation(relName, relNum);

        // reading page1
        ReadPage(relNum, 0);
        pageBuffer[relNum].pid = 0;
        pageBuffer[relNum].dirty = 0;

        // Now this relNum is occupied by given relation, pageValid is set to 1
        pageValid[relNum] = 1;
        return relNum;
    } else {
        ErrorMsgs(REL_NOEXIST, 1);
        return NOTOK;
    }
}


/*
 *
 * Creates relCache entries for relation dynamically
 */
int CreateCacheEntryForRelation(char *relName, int relNum) {

    Rid startRid, foundRid;
    startRid.pid = 0;
    startRid.slotnum = 0;
    int attrType = 0, compOp = 501;
    int attrSize = RELNAME_SIZE;
    int attrOffset = getattrOffset(0, "relationName");

    relRecord sourceRecord, foundRecord;
    strcpy(sourceRecord.relationName, relName);

    // Fetching a Relcat Record corresponding to this Relation
    int valid = FindRec(0, &startRid, &foundRid, (char *) &foundRecord, attrType, attrSize, attrOffset,
                        (char *) &sourceRecord, compOp);

    strcpy(relCache[relNum].relationName, foundRecord.relationName);
    relCache[relNum].recordLength = foundRecord.recordLength;
    relCache[relNum].recordsPerPage = foundRecord.recordsPerPage;
    relCache[relNum].attributeCount = foundRecord.attributeCount;
    relCache[relNum].recordCount = foundRecord.recordCount;
    relCache[relNum].pageCount = foundRecord.pageCount;
    relCache[relNum].slotBitmapSize = getSlotMapSize(foundRecord.recordLength);
    updateTimestamp(relNum);
    relCache[relNum].relcatRid.pid = foundRid.pid;
    relCache[relNum].relcatRid.slotnum = foundRid.slotnum;
    relCache[relNum].dirty = 0;
    relCache[relNum].filePtr = fopen(getFileName(relName), "r+");
    relCache[relNum].attrList = getAttributeList(relName);
}


/*
 *
 * returns offset of attribute in raw byte array of tuple
 */
int getattrOffset(int relNum, char *attrName) {
    attrCatNode *temp;
    temp = relCache[relNum].attrList;

    while (temp) {
        if (!strcmp(temp->attrName, attrName)) {
            return temp->attrOffset;
        }
        temp = temp->next;
    }
}
