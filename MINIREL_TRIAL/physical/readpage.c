#include "../include/defs.h"

/*------------------------------------------------------------
FUNCTION ReadPage(int relNum, int pid)

PARAMETER DESCRIPTION:
       relNum   : slot number in relCache
       pid      : required page Number

ERRORS REPORTED:
       RELCAT_NOT_LOADED
       ATTRCAT_NOT_LOADED
       RELATION_NOT_LOADED

IMPLEMENTATION NOTES (IF ANY):
       This function reads required page number for given relation.

------------------------------------------------------------*/
int ReadPage(int relNum, int pid) {

    char fileName[MAX_RELNAME_SIZE];

    if (relNum == 0)
        strcpy(fileName, getFileName(RELCAT));
    else if (relNum == 1)
        strcpy(fileName, getFileName(ATTRCAT));
    else
        strcpy(fileName, getFileName(relCache[relNum].relationName));

    FILE *file = fopen(fileName, "r");

    // seek File pointer offset to desired page
    fseek(file, pid * PAGESIZE, SEEK_SET);

    if (fread(pageBuffer[relNum].pageData, 1, PAGESIZE, file) < PAGESIZE) {
        if (relNum == 0) {
            ErrorMsgs(RELCAT_NOT_LOADED, 1);
        } else if (relNum == 1) {
            ErrorMsgs(ATTRCAT_NOT_LOADED, 1);
        } else {
            ErrorMsgs(RELATION_NOT_LOADED, 1);
        }
        return NOTOK;
    }

    // new page is not dirty
    pageBuffer[relNum].pid = pid;
    pageBuffer[relNum].dirty = 0;

    fclose(file);
}
