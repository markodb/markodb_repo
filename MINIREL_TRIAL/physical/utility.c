#include <time.h>
#include "../include/defs.h"


/*
 * returns count of maximum tuple possible in a page
 */
int getTotalSlots(int tupleSize) {
    return (int) ((PAGESIZE - getSlotMapSize(tupleSize)) / tupleSize);
}


/*
 * returns count of bytes required to store SlotMap for page
 */
int getSlotMapSize(int tupleSize) {
    return (int) (PAGESIZE / ((8 * tupleSize) + 1)) + 1;    //return BitMap size in Bytes
}


/*
 * returns record length of relCat tuple
 */
size_t getRelcatRecordLength() {
    return getRecordLengthForCreate(getRelcatAttributes());
}

/*
 * returns record length of attrCat tuple
 */
size_t getAttrcatRecordLength() {
    return getRecordLengthForCreate(getAttrcatAttributes());
}


/*
 * returns record length of any attribute list
 */
size_t getRelationRecordLength(char *argv[]) {
    return getRecordLengthForCreate(argv);
}


/*
 * returns absolute path name of a file
 */
char *getFileName(char *name) {
    static char filePath[PATH_SIZE];
    strcpy(filePath, currOpenDB);
    strcat(filePath, "/");
    strcat(filePath, name);
    return filePath;
}

/*
 * statically defining schema for RELCAT
 */
char **getRelcatAttributes() {
    char **array = (char **) malloc(ATTR_COUNT * sizeof(char *));
    int i;

    for (i = 0; i < ATTR_COUNT; i++)
        array[i] = (char *) malloc(MAX_ATTRNAME_SIZE);

    strcpy(array[0], "create");
    strcpy(array[1], RELCAT);
    strcpy(array[2], "recordLength");
    strcpy(array[3], "i");
    strcpy(array[4], "recordsPerPage");
    strcpy(array[5], "i");
    strcpy(array[6], "attributeCount");
    strcpy(array[7], "i");
    strcpy(array[8], "recordCount");
    strcpy(array[9], "i");
    strcpy(array[10], "pageCount");
    strcpy(array[11], "i");
    strcpy(array[12], "relationName");
    strcpy(array[13], "s20");
    strcpy(array[14], "NIL");

    return array;
}


/*
 * statically defining schema for ATTRCAT
 */
char **getAttrcatAttributes() {
    char **array = (char **) malloc(ATTR_COUNT * sizeof(char *));
    int i;

    for (i = 0; i < ATTR_COUNT; i++)
        array[i] = (char *) malloc(MAX_ATTRNAME_SIZE);

    strcpy(array[0], "create");
    strcpy(array[1], ATTRCAT);
    strcpy(array[2], "attrOffset");
    strcpy(array[3], "i");
    strcpy(array[4], "attrLength");
    strcpy(array[5], "i");
    strcpy(array[6], "attrType");
    strcpy(array[7], "i");
    strcpy(array[8], "attrName");
    strcpy(array[9], "s20");
    strcpy(array[10], "relationName");
    strcpy(array[11], "s20");
    strcpy(array[12], "NIL");

    return array;
}


/*
 * Returns record length for given schema[argv] in Create function.
 */
size_t getRecordLengthForCreate(char *argv[]) {
    size_t recordLength = 0;
    int argc = getARGVcount(argv);
    for (int i = 3; i <= argc - 1; i = i + 2) {
        if (argv[i][0] == 'i' || argv[i][0] == 'f') {
            recordLength += 4;
        } else if (argv[i][0] == 's') {
            recordLength += atoi(argv[i] + 1);
        }
    }
    return recordLength;
}


/*
 * Returns pointer to attrCat records array at the time of Relation creation.
 * These attrCat records will be inserted in Attrcat relation.
 */
attrRecord *setAttrFields(char *argv[]) {
    int totalAttributes = (getARGVcount(argv) - 2) / 2;
    attrRecord *attrCatObject;

    // allocating memory for attrCat Records
    attrCatObject = (attrRecord *) malloc(sizeof(attrRecord) * totalAttributes);

    // setting offset = 0  for first attribute
    attrCatObject[0].attrOffset = 0;

    for (int i = 0; i < totalAttributes; i++) {

        // setting attrName
        strncpy(attrCatObject[i].attrName, argv[i * 2 + 2], strlen(argv[i * 2 + 2]) + 1);

        // setting attrCatObject[i].attrType and attrLength for a particular attribute
        if (argv[i * 2 + 3][0] == 's') {
            attrCatObject[i].attrType = 0;   // 1 for integer, 0 for string, 2 for float
            attrCatObject[i].attrLength = atoi(argv[i * 2 + 3] + 1);
        } else if (argv[i * 2 + 3][0] == 'i') {
            attrCatObject[i].attrType = 1;
            attrCatObject[i].attrLength = 4;
        } else if (argv[i * 2 + 3][0] == 'f') {
            attrCatObject[i].attrType = 2;
            attrCatObject[i].attrLength = 4;
        }

        // setting attrCatObject[i].attrOffset
        if (i > 0) {
            attrCatObject[i].attrOffset = attrCatObject[i - 1].attrOffset + attrCatObject[i - 1].attrLength;
        }

        // setting relationName
        strncpy(attrCatObject[i].relationName, argv[1], strlen(argv[1]) + 1);
    }

    return attrCatObject;
}


/*
 * returns Relnum if slot is available relcache
 * otherwise uses LRUFinder to replace a reation in relcache
 *
 */
int getEmptyBufferSlot() {

    int i = 0;

    for (i = 2; i < MAXOPEN; i++)
        if (pageValid[i] == 0)
            return i;

    return LRUpageFinder();
}


/*
 * LRU page replacer
 */
int LRUpageFinder() {

    int i = 0;
    int toReplace = 2;

    for (i = 2; i < MAXOPEN; i++) {
        if (pageValid[i] == 1) {
            if (relCache[i].time.tv_sec < relCache[toReplace].time.tv_sec) {
                toReplace = i;
            } else if (relCache[i].time.tv_sec == relCache[toReplace].time.tv_sec &&
                       relCache[i].time.tv_usec < relCache[toReplace].time.tv_usec) {
                toReplace = i;
            }
        }
    }

    // closing previously opened table
    CloseRel(toReplace);

    return toReplace;
}


/*
 * update timeStamp of relCat slot
 */
void updateTimestamp(int relnum) {

    struct timeval tv;
    gettimeofday(&tv, NULL);

    relCache[relnum].time.tv_sec = tv.tv_sec;
    relCache[relnum].time.tv_usec = tv.tv_usec;

}


/*
 *
 * deletes RELCAT Record for a given relation name
 *
 */
void DeleteRelcatRecord(char *relationName) {
    Rid startRid, foundRid;
    startRid.pid = 0;
    startRid.slotnum = 0;
    int attrType = 0, compOp = 501;
    int attrSize = RELNAME_SIZE;
    int attrOffset = getattrOffset(0, "relationName");

    relRecord sourceRecord, foundRecord;
    strcpy(sourceRecord.relationName, relationName);
    FindRec(0, &startRid, &foundRid, (char *) &foundRecord, attrType, attrSize, attrOffset, (char *) &sourceRecord,
            compOp);

    DeleteRec(0, &foundRid);
}

/*
 *
 * deletes all ATTRCAT Records for a given relation name
 *
 */
void DeleteAttrcatRecords(char *relationName) {
    Rid startRid, foundRid;
    startRid.pid = 0;
    startRid.slotnum = 0;
    int attrType = 0, compOp = 501;
    int attrSize = RELNAME_SIZE;
    int attrOffset = getattrOffset(1, "relationName");

    attrRecord sourceRecord, foundRecord;
    strcpy(sourceRecord.relationName, relationName);
    while (1) {
        if (FindRec(1, &startRid, &foundRid, (char *) &foundRecord, attrType, attrSize, attrOffset,
                    (char *) &sourceRecord, compOp) == OK) {
            DeleteRec(1, &foundRid);

            if (foundRid.slotnum == relCache[1].recordsPerPage - 1 && foundRid.pid + 1 < relCache[1].pageCount) {
                // when foundRid is last slotNo and there exist next page
                startRid.slotnum = 0;
                startRid.pid = foundRid.pid + 1;
            } else if (foundRid.slotnum < relCache[1].recordsPerPage - 1) {
                // when foundRid is not last slotNo and no need to check if there exist next page
                startRid.slotnum = foundRid.slotnum + 1;
                startRid.pid = foundRid.pid;
            } else {
                // when foundRid is last slotNo and there doesn't exist next page
                break;
            }
        } else {
            break;
        }

    }
}


int isValueString(char *str) {

    int i;
    if (strlen(str) < 50 && isalpha(str[0]) != 0) {
        for (i = 1; i < strlen(str); i++) {
            if (!(isalnum(str[i])) && str[i] != ' ')
                return NOTOK;
        }
    } else {
        return NOTOK;
    }// length must be less than 50 and// first letter must be non null character
    return OK;
}


int isValueInteger(char *str) {

    int i, start = 0;

    if (str[0] == '-')
        start = 1;

    for (i = start; str[i] != '\0'; i++) {
        if (isdigit(str[i]) == 0 || str[i] == '.')                        // all characters must be digits only
            return NOTOK;
    }
    return OK;
}


int isValueFloat(char *str) {
    int i, start = 0, decPointCount = 0;

    if (str[0] == '-')
        start = 1;

    for (i = start; str[i] != '\0'; i++) {
        if (isdigit(str[i]) == 0 && str[i] != '.')                        // all characters must be digits only
            return NOTOK;
        else if (str[i] == '.' && decPointCount == 0)           // when decimal point appear first time,OK
            decPointCount++;
        else if (str[i] == '.' && decPointCount == 1)           // when decimal point appear first time,NOT OK
            return NOTOK;
    }
    return OK;
}

// validates attribute value against attrType
int validateAttrValues(int attrType, char *value) {
    switch (attrType) {
        case 0:
            return isValueString(value);
        case 1:
            return isValueInteger(value);
        case 2:
            return isValueFloat(value);
        default: return NOTOK;
    }
}


// returns OK if db is open
int isDBOpen() {
    if (!strcmp(currOpenDB, MINIREL_HOME)) {
        return NOTOK;
    }
    return OK;
}

// initializes all globals at the starting of MINIREL
void initialize_database() {
    struct stat st;
    char logFilePath[PATH_SIZE];

    strcpy(MINIREL_HOME, getenv("HOME"));
    strcat(MINIREL_HOME,"/");
    strcat(MINIREL_HOME,"MINIREL");

    strcpy(logFilePath, MINIREL_HOME);
    strcat(logFilePath,"/");
    strcat(logFilePath,"MINIREL.log");

    // creating MINIREL_HOME if not created already
    if (stat(MINIREL_HOME, &st) == -1) {
        if (mkdir(MINIREL_HOME, 0700) == -1)
            ErrorMsgs(UNABLE_CREATE_MINIREL_HOME, 1);
    }
    // creating log file after creating MINIREL_HOME

    logFilePtr = fopen(logFilePath,"a");
    isSelfJoin = 0;
    SetDBPath(MINIREL_HOME);

    LOG(">>> STARTING MINIREL SESSION <<<");
}


int getARGVcount(char *array[]) {
    int i, count = 0;

    for (i = 0;; i++) {
        if (!strcmp(array[i], "NIL")) {
            return count;
        }
        count++;
    }
}


/*
 * fetch current date/time in currTime
 */
void getTime() {
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    currTime[0] = '\0';
    strftime(currTime, sizeof(currTime), "%c", tm);
}


/*
 * form log msg
 */
void formLogMsg(char * msg){
    LogMsg[0] = '\0';
    strcpy(LogMsg, currTime);
    strcat(LogMsg, " : ");
    strcat(LogMsg, msg);
    strcat(LogMsg, "\n");
}


/*
 * function for writing log msg
 */
void write_log(char *msg){
    getTime();
    formLogMsg(msg);
    fwrite(LogMsg,strlen(LogMsg),1,logFilePtr);
    fflush(logFilePtr);
}