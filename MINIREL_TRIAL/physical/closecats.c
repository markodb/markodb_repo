#include "../include/defs.h"


/*------------------------------------------------------------

FUNCTION CloseCats()

IMPLEMENTATION NOTES (IF ANY):
       When Database closes, it closes all opened catalogs.

------------------------------------------------------------*/
void CloseCats() {
    // first closing opened relations
    for (int i = 2; i < MAXOPEN; i++) {
        if (pageValid[i] == 1) {
            CloseRel(i);
        }
    }
    // closing attrcat and relcat respectively
    CloseRel(1);
    CloseRel(0);
}
