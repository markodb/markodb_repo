/************************INCLUDES*******************************/
#include "../include/error.h"
#include "../include/defs.h"

/*------------------------------------------------------------

FUNCTION ErrorMsgs(int errorNum, int printFlag)

PARAMETER DESCRIPTION:
       errorNum  : Error Number corresponding to Raised Error
       printFlag : 1- print error/ 0- don't print error

IMPLEMENTATION NOTES (IF ANY):
       Prints Errors on violations.

------------------------------------------------------------*/

int ErrorMsgs(int errorNum, int printFlag) {
    if (!printFlag) {
        return errorNum;
    }

    switch (errorNum) {
        case REL_NOEXIST:
            printf(RED "\nError: Relation doesn't exist" RESET);
            LOG("Error: Relation doesn't exist");
            break;
        case ATTR_NOEXIST:
            printf(RED "\nError: Attribute doesn't exist" RESET);
            LOG("Error: Attribute doesn't exist");
            break;
        case DB_NOT_EXIST:
            printf(RED "\nError: Database doesn't exist" RESET);
            LOG("Error: Database doesn't exist");
            break;
        case COMMON_ATTR_NOEXIST:
            printf(RED "\nError: Common attribute doesn't exist" RESET);
            LOG("Error: Common attribute doesn't exist");
            break;
        case ATTR_NAME_LENGTH_EXCEED:
            printf(RED "\nError: Attribute name length exceed 20" RESET);
            LOG("Error: Attribute name length exceed 20");
            break;
        case REL_NAME_LENGTH_EXCEED:
            printf(RED "\nError: Relation name length exceed 20" RESET);
            LOG("Error: Relation name length exceed 20");
            break;
        case MAX_CHAR_STR_LENGTH_EXCEED:
            printf(RED "\nError: Character string length exceed 50" RESET);
            LOG("Error: Character string length exceed 50");
            break;
        case CATALOGS_CREATION_FAILED:
            printf(RED "\nError: Catalogs creation failed" RESET);
            LOG("Error: Catalogs creation failed");
            break;
        case RELCAT_ALREADY_EXIST:
            printf(RED "\nError: Unable to create relCat as it is already exist" RESET);
            LOG("Error: Unable to create relCat as it already exist");
            break;
        case ATTRCAT_ALREADY_EXIST:
            printf(RED "\nError: Unable to create attrCat as it is already exist" RESET);
            LOG("Error: Unable to create attrCat as it already exist");
            break;
        case DB_ALREADY_EXIST:
            printf(RED "\nError: DataBase creation failed, DataBase with same name already exist" RESET);
            LOG("Error: DataBase creation failed, DataBase with same name already exist");
            break;
        case RELATION_NOT_LOADED:
            printf(RED "\nError: Unable to load relation into buffers and cache" RESET);
            LOG("Error: Unable to load relation into buffers and cache");
            break;
        case RELATION_ALREADY_EXIST:
            printf(RED "\nError: Unable to create relation as it already exist" RESET);
            LOG("Error: Unable to create relation as it already exist");
            break;
        case UNABLE_TO_DELETE_REL:
            printf(RED "\nError: Unable to delete relation, Relation doesn't exist" RESET);
            LOG("Error: Unable to delete relation, Relation doesn't exist");
            break;
        case UNABLE_TO_INSERT:
            printf(RED "\nError: Denied to insert a record in closed relation, Open relation before insertion" RESET);
            LOG("Error: Denied to insert a record in closed relation, Open relation before insertion");
            break;
        case INVALID_ATTR_LIST:
            printf(RED "\nError: Attribute value type conflicting with schema" RESET);
            LOG("Error: Attribute value type conflicting with schema");
            break;
        case DESTINATION_RELATION_EXIST:
            printf(RED "\nError: Destination relation already exist, provide another unique name" RESET);
            LOG("Error: Destination relation already exist, provide another unique name");
            break;
        case SOURCE_RELATION_NOT_EXIST:
            printf(RED "\nError: Source relation doesn't exist" RESET);
            LOG("Error: Source relation doesn't exist");
            break;
        case TUPLE_ALREADY_EXIST:
            printf(RED "\nError: Tuple already exist in relation" RESET);
            LOG("Error: Tuple already exist in relation");
            break;
        case UNABLE_TO_FLUSH_PAGE:
            printf(RED "\nError: Failed to flush page to disk file" RESET);
            LOG("Error: Failed to flush page to disk file");
            break;
        case ATTR_VALUES_NOT_VALID:
            printf(RED "\nError: Attribute value provided is invalid" RESET);
            LOG("Error: Attribute value provided is invalid");
            break;
        case ATTR_NAME_NOT_VALID:
            printf(RED "\nError: Attribute Name provided is invalid" RESET);
            LOG("Error: Attribute Name provided is invalid");
            break;
        case CANNOT_LOAD_FILE:
            printf(RED "\nError: Cannot load relation, relation not created " RESET);
            LOG("Error: Cannot load relation, relation not created ");
            break;
        case CANT_LOAD_REL_NOT_EMPTY:
            printf(RED "\nError: Can't load file, relation not empty" RESET);
            LOG("Error: Can't load file, relation not empty");
            break;
        case OPEN_REL_BEFORE_PRINT:
            printf(RED "\nError: Open the relation, before print command" RESET);
            LOG("Error: Open the relation, before print command");
            break;
        case DB_NOT_OPEN:
            printf(RED "\nError: Open the database, before any operation" RESET);
            LOG("Error: Open the database, before any operation");
            break;
        case USER_CANT_DESTROY_METADATA:
            printf(RED "\nError: Denied to delete metadata" RESET);
            LOG("Error: Denied to delete metadata");
            break;
        case USER_CANT_OPEN_METADATA:
            printf(RED "\nError: Denied to view metadata" RESET);
            LOG("Error: Denied to view metadata");
            break;
        case CANT_CREATE_NESTED_DB:
            printf(RED "\nError: Denied to create nested DB" RESET);
            LOG("Error: Denied to create nested DB");
            break;
        case CANT_OPEN_NESTED_DB:
            printf(RED "\nError: Denied to open another DB inside opened DB" RESET);
            LOG("Error: Denied to open another DB inside opened DB");
            break;
        case CANT_DELETE_OPENED_DB:
            printf(RED "\nError: Denied to delete opened DB" RESET);
            LOG("Error: Denied to delete opened DB");
            break;
        case CANT_DELETE_NON_EXIST_DB:
            printf(RED "\nError: DB don't exist, denied to delete DB" RESET);
            LOG("Error: DB don't exist, denied to delete DB");
            break;
        case INVALID_ATTR_COUNT:
            printf(RED "\nError: Attribute list count does't match with the schema of relation" RESET);
            LOG("Error: Attribute list count does't match with the schema of relation");
            break;
        case UNABLE_CREATE_MINIREL_HOME:
            printf(RED "\nError: Unable to create MINIREL_HOME" RESET);
            LOG("Error: Unable to create MINIREL_HOME");
            break;
        case JOIN_COLUMN_NAME_MISMATCH:
            printf(RED "\nError: Join column name mismatch among joining relation" RESET);
            LOG("Error: Join column name mismatch among joining relation");
            break;
        case JOIN_COL_MISMATCH_SCHMA_COL:
            printf(RED "\nError: Join column name mismatch with schema" RESET);
            LOG("Error: Join column name mismatch with schema");
            break;
        case DELETE_COL_MISMATCH:
            printf(RED "\nError: Delete column is not matching with schema" RESET);
            LOG("Error: Delete column is not matching with schema");
            break;
        case DELETE_COL_VALUE_MISMATCH:
            printf(RED "\nError: Delete column value data type conflicting with schema" RESET);
            LOG("Error: Delete column value data type conflicting with schema");
            break;
        case LOAD_FILE_MISSING:
            printf(RED "\nError: Load file missing on specified path" RESET);
            LOG("Error: Load file missing on specified path");
            break;
        case USER_CANT_UPDATE_METADATA:
            printf(RED "\nError: Denied to load metadata" RESET);
            LOG("Error: Denied to load metadata");
            break;
        case SELECT_COL_MISMATCH_SCHMA_COL:
            printf(RED "\nError: Join column name mismatch with schema" RESET);
            LOG("Error: Join column name mismatch with schema");
            break;
        case USER_CANT_PRINT_METADATA:
            printf(RED "\nError: Denied to print metadata" RESET);
            LOG("Error: Denied to print metadata");
            break;
        case USER_CANT_CREATE_METADATA:
            printf(RED "\nError: Denied to create metadata relations" RESET);
            LOG("Error: Denied to create metadata relations");
            break;

    }
    return errorNum;
}

