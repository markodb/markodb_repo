#include "../include/defs.h"

/*------------------------------------------------------------

FUNCTION CloseRel(int relNum)

PARAMETER DESCRIPTION:
       relNum : index number in relCache

IMPLEMENTATION NOTES (IF ANY):
       Invoked by closecats() for all opened Relations.
       If relCache page is dirty, then flush that also.

------------------------------------------------------------*/
void CloseRel(int relNum) {
    FlushPage(relNum);

    // Updating Relcat record if it is dirty
    if (relCache[relNum].dirty == 1) {
        relRecord relRecord1;
        relRecord1.recordLength = relCache[relNum].recordLength;
        relRecord1.recordsPerPage = relCache[relNum].recordsPerPage;
        relRecord1.recordCount = relCache[relNum].recordCount;
        relRecord1.pageCount = relCache[relNum].pageCount;
        relRecord1.attributeCount = relCache[relNum].attributeCount;
        strcpy(relRecord1.relationName, relCache[relNum].relationName);

        // STEP 1    it writes a relCat record in PageBuffer[0] i.e Relcat relation page buffer
        WriteRec(0, (char *) &relRecord1, relCache[relNum].relcatRid);
    }

    // required to flush pagebuffer of Relcat because of STEP 1, only if relNum is 0
    //if (relNum == 0) {
        FlushPage(0);
    //}

    freeListMemory(relCache[relNum].attrList);

    fclose(relCache[relNum].filePtr);
    pageValid[relNum] = 0;

    return;
}


/*
 *
 * Free malloc memory blocks for attribute list
 */

void freeListMemory(attrCatNode *head) {

    attrCatNode *shadow;
    while (head) {
        shadow = head;
        head = head->next;
        free(shadow);
    }
}